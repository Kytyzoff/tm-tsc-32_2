package ru.tsc.borisyuk.tm.client;

import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.SneakyThrows;
import ru.tsc.borisyuk.tm.api.client.IProjectEndpointClient;
import ru.tsc.borisyuk.tm.dto.request.*;
import ru.tsc.borisyuk.tm.dto.response.*;

@NoArgsConstructor
public final class ProjectEndpointClient extends AbstractEndpointClient implements IProjectEndpointClient {

    public ProjectEndpointClient(@NonNull final AbstractEndpointClient client) {
        super(client);
    }

    @NonNull
    @Override
    @SneakyThrows
    public ProjectChangeStatusByIdResponse projectChangeStatusById(@NonNull final ProjectChangeStatusByIdRequest request) {
        return call(request, ProjectChangeStatusByIdResponse.class);
    }

    @NonNull
    @Override
    @SneakyThrows
    public ProjectChangeStatusByIndexResponse projectChangeStatusByIndex(@NonNull final ProjectChangeStatusByIndexRequest request) {
        return call(request, ProjectChangeStatusByIndexResponse.class);
    }

    @NonNull
    @Override
    @SneakyThrows
    public ProjectRemoveByIdResponse projectRemoveById(@NonNull final ProjectRemoveByIdRequest request) {
        return call(request, ProjectRemoveByIdResponse.class);
    }

    @NonNull
    @Override
    @SneakyThrows
    public ProjectRemoveByIndexResponse projectRemoveByIndex(@NonNull final ProjectRemoveByIndexRequest request) {
        return call(request, ProjectRemoveByIndexResponse.class);
    }

    @NonNull
    @Override
    @SneakyThrows
    public ProjectGetByIdResponse projectGetById(@NonNull final ProjectGetByIdRequest request) {
        return call(request, ProjectGetByIdResponse.class);
    }

    @NonNull
    @Override
    @SneakyThrows
    public ProjectGetByIndexResponse projectGetByIndex(@NonNull final ProjectGetByIndexRequest request) {
        return call(request, ProjectGetByIndexResponse.class);
    }

    @NonNull
    @Override
    @SneakyThrows
    public ProjectUpdateByIdResponse projectUpdateById(@NonNull final ProjectUpdateByIdRequest request) {
        return call(request, ProjectUpdateByIdResponse.class);
    }

    @NonNull
    @Override
    @SneakyThrows
    public ProjectUpdateByIndexResponse projectUpdateByIndex(@NonNull final ProjectUpdateByIndexRequest request) {
        return call(request, ProjectUpdateByIndexResponse.class);
    }

    @NonNull
    @Override
    @SneakyThrows
    public ProjectCreateResponse projectCreate(@NonNull final ProjectCreateRequest request) {
        return call(request, ProjectCreateResponse.class);
    }

    @NonNull
    @Override
    @SneakyThrows
    public ProjectListResponse projectList(@NonNull final ProjectListRequest request) {
        return call(request, ProjectListResponse.class);
    }

    @NonNull
    @Override
    @SneakyThrows
    public ProjectClearResponse projectClear(@NonNull final ProjectClearRequest request) {
        return call(request, ProjectClearResponse.class);
    }

}

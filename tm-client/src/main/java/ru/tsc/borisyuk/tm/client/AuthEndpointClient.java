package ru.tsc.borisyuk.tm.client;

import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.SneakyThrows;
import ru.tsc.borisyuk.tm.api.client.IAuthEndpointClient;
import ru.tsc.borisyuk.tm.dto.request.UserLoginRequest;
import ru.tsc.borisyuk.tm.dto.request.UserLogoutRequest;
import ru.tsc.borisyuk.tm.dto.request.UserProfileRequest;
import ru.tsc.borisyuk.tm.dto.response.UserLoginResponse;
import ru.tsc.borisyuk.tm.dto.response.UserLogoutResponse;
import ru.tsc.borisyuk.tm.dto.response.UserProfileResponse;

@NoArgsConstructor
public final class AuthEndpointClient extends AbstractEndpointClient implements IAuthEndpointClient {

    public AuthEndpointClient(@NonNull final AbstractEndpointClient client) {
        super(client);
    }

    public static void main(String[] args) {
        AuthEndpointClient client = new AuthEndpointClient();
        client.connect();
        AuthEndpointClient client2 = new AuthEndpointClient(client);
        System.out.println(client.profile(new UserProfileRequest()).getUser());
        System.out.println(client.login(new UserLoginRequest("test2", "test2")).getSuccess());
        System.out.println(client.login(new UserLoginRequest("test", "test")).getSuccess());
        System.out.println(client.profile(new UserProfileRequest()).getUser().getEmail());
        System.out.println(client2.profile(new UserProfileRequest()).getUser().getEmail());
        System.out.println(client.logout(new UserLogoutRequest()));
        System.out.println(client.profile(new UserProfileRequest()).getUser());
        client.disconnect();
    }

    @NonNull
    @Override
    @SneakyThrows
    public UserLoginResponse login(@NonNull final UserLoginRequest request) {
        return call(request, UserLoginResponse.class);
    }

    @NonNull
    @Override
    @SneakyThrows
    public UserLogoutResponse logout(@NonNull final UserLogoutRequest request) {
        return call(request, UserLogoutResponse.class);
    }

    @NonNull
    @Override
    @SneakyThrows
    public UserProfileResponse profile(@NonNull final UserProfileRequest request) {
        return call(request, UserProfileResponse.class);
    }

}

package ru.tsc.borisyuk.tm.command.task;

import lombok.NonNull;
import ru.tsc.borisyuk.tm.dto.request.TaskUpdateByIndexRequest;
import ru.tsc.borisyuk.tm.util.TerminalUtil;

public final class TaskUpdateByIndexCommand extends AbstractTaskCommand {

    @NonNull
    public static final String NAME = "task-update-by-index";

    @NonNull
    public static final String DESCRIPTION = "Update task by index.";

    public static final String ARGUMENT = null;

    @NonNull
    @Override
    public String getName() {
        return NAME;
    }

    @NonNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NonNull final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER NAME:");
        @NonNull final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @NonNull final String description = TerminalUtil.nextLine();
        @NonNull final TaskUpdateByIndexRequest request = new TaskUpdateByIndexRequest();
        request.setIndex(index);
        request.setName(name);
        request.setDescription(description);
        getTaskEndpoint().taskUpdateByIndex(request);
    }

}

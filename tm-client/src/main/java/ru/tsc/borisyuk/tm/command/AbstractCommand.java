package ru.tsc.borisyuk.tm.command;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import ru.tsc.borisyuk.tm.api.model.ICommand;
import ru.tsc.borisyuk.tm.enumerated.Role;
import ru.tsc.borisyuk.tm.api.service.IServiceLocator;

public abstract class AbstractCommand implements ICommand {

    @Getter
    @Setter
    protected IServiceLocator serviceLocator;

    public abstract void execute();

    public abstract Role[] getRoles();

    @Override
    public String toString() {
        final String name = getName();
        final String description = getDescription();
        final String argument = getArgument();
        @NonNull String result = "";
        if (StringUtils.isNotEmpty(name)) result += name + " : ";
        if (StringUtils.isNotEmpty(argument)) result += argument + " : ";
        if (StringUtils.isNotEmpty(description)) result += description;
        return result;
    }

}

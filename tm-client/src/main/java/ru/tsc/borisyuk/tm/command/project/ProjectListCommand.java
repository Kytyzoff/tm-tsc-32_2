package ru.tsc.borisyuk.tm.command.project;

import lombok.NonNull;
import ru.tsc.borisyuk.tm.dto.request.ProjectListRequest;
import ru.tsc.borisyuk.tm.dto.response.ProjectListResponse;
import ru.tsc.borisyuk.tm.model.Project;
import ru.tsc.borisyuk.tm.client.ProjectEndpointClient;
import ru.tsc.borisyuk.tm.enumerated.Sort;
import ru.tsc.borisyuk.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class ProjectListCommand extends AbstractProjectCommand {

    @NonNull
    public static final String NAME = "project-list";

    @NonNull
    public static final String DESCRIPTION = "Show project list.";

    public static final String ARGUMENT = null;

    @NonNull
    @Override
    public String getName() {
        return NAME;
    }

    @NonNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.asList(Sort.values()));
        @NonNull final String sortValue = TerminalUtil.nextLine();
        final Sort sort = Sort.toSort(sortValue);
        @NonNull final ProjectEndpointClient projectEndpoint = getServiceLocator().getProjectEndpoint();
        final ProjectListRequest request = new ProjectListRequest();
        request.setSort(sort);
        @NonNull final ProjectListResponse projectListResponse = projectEndpoint.projectList(request);
        final List<Project> projects = projectListResponse.getProjects();
        int index = 1;
        for (final Project project : projects) {
            if (project == null) continue;
            System.out.println(index + ". " + project);
            index++;
        }
    }

}

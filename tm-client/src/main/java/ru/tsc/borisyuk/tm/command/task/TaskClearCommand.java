package ru.tsc.borisyuk.tm.command.task;

import lombok.NonNull;
import ru.tsc.borisyuk.tm.dto.request.TaskClearRequest;

public final class TaskClearCommand extends AbstractTaskCommand {

    @NonNull
    public static final String NAME = "task-clear";

    @NonNull
    public static final String DESCRIPTION = "Clear task list.";

    public static final String ARGUMENT = null;

    @NonNull
    @Override
    public String getName() {
        return NAME;
    }

    @NonNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[TASK CLEAR]");
        @NonNull final TaskClearRequest request = new TaskClearRequest();
        getTaskEndpoint().taskClear(request);
    }

}

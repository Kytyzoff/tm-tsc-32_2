package ru.tsc.borisyuk.tm.command.task;

import lombok.Getter;
import lombok.NonNull;
import ru.tsc.borisyuk.tm.dto.request.TaskChangeStatusByIdRequest;
import ru.tsc.borisyuk.tm.dto.request.TaskChangeStatusByIndexRequest;
import ru.tsc.borisyuk.tm.enumerated.Status;
import ru.tsc.borisyuk.tm.util.TerminalUtil;

import java.util.Arrays;

@Getter
public final class TaskChangeStatusByIndexCommand extends AbstractTaskCommand {

    @NonNull
    public static final String NAME = "task-change-status-by-index";

    @NonNull
    public static final String DESCRIPTION = "Change task status by index.";

    public static final String ARGUMENT = null;

    @NonNull
    @Override
    public String getName() {
        return NAME;
    }

    @NonNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[CHANGE TASK STATUS BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NonNull final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.asList(Status.values()));
        @NonNull final String statusValue = TerminalUtil.nextLine();
        @NonNull final Status status = Status.toStatus(statusValue);
        @NonNull final TaskChangeStatusByIndexRequest request = new TaskChangeStatusByIndexRequest();
        request.setIndex(index);
        request.setStatus(status);
        getTaskEndpoint().taskChangeStatusByIndex(request);
    }

}

package ru.tsc.borisyuk.tm.command.user;

import lombok.NonNull;
import ru.tsc.borisyuk.tm.dto.request.UserLoginRequest;
import ru.tsc.borisyuk.tm.dto.request.UserLogoutRequest;
import ru.tsc.borisyuk.tm.enumerated.Role;

public final class UserLogoutCommand extends AbstractUserCommand {

    @NonNull
    public static final String NAME = "logout";

    @NonNull
    public static final String DESCRIPTION = "Logout user.";

    public static final String ARGUMENT = null;

    @NonNull
    @Override
    public String getName() {
        return NAME;
    }

    @NonNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[USER LOGOUT]");
        @NonNull final UserLogoutRequest request = new UserLogoutRequest();
        getAuthEndpoint().logout(request);
    }

    @NonNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}

package ru.tsc.borisyuk.tm.command.user;

import lombok.NonNull;
import ru.tsc.borisyuk.tm.dto.request.UserUpdateProfileRequest;
import ru.tsc.borisyuk.tm.enumerated.Role;
import ru.tsc.borisyuk.tm.util.TerminalUtil;

public final class UserUpdateProfileCommand extends AbstractUserCommand {

    @NonNull
    public static final String NAME = "user-update-profile";

    @NonNull
    public static final String DESCRIPTION = "Update user profile.";

    public static final String ARGUMENT = null;

    @NonNull
    @Override
    public String getName() {
        return NAME;
    }

    @NonNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE USER PROFILE]");
        System.out.println("ENTER FIRST NAME:");
        @NonNull final String firstName = TerminalUtil.nextLine();
        System.out.println("ENTER LAST NAME:");
        @NonNull final String lastName = TerminalUtil.nextLine();
        System.out.println("ENTER MIDDLE NAME:");
        @NonNull final String middleName = TerminalUtil.nextLine();
        @NonNull UserUpdateProfileRequest request = new UserUpdateProfileRequest();
        request.setFirstName(firstName);
        request.setLastName(lastName);
        request.setMiddleName(middleName);
        getUserEndpoint().updateUserProfile(request);
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}

package ru.tsc.borisyuk.tm.command.user;

import lombok.NonNull;
import ru.tsc.borisyuk.tm.dto.request.UserRegisterRequest;
import ru.tsc.borisyuk.tm.enumerated.Role;
import ru.tsc.borisyuk.tm.util.TerminalUtil;

public final class UserRegisterCommand extends AbstractUserCommand {

    @NonNull
    public static final String NAME = "user-register";

    @NonNull
    public static final String DESCRIPTION = "Register new user.";

    public static final String ARGUMENT = null;

    @NonNull
    @Override
    public String getName() {
        return NAME;
    }

    @NonNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[REGISTER NEW USER]");
        System.out.println("ENTER LOGIN:");
        @NonNull final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        @NonNull final String password = TerminalUtil.nextLine();
        System.out.println("ENTER EMAIL:");
        @NonNull final String email = TerminalUtil.nextLine();
//        getServiceLocator().getUserEndpoint().registerUser(new UserRegisterRequest(login, password, email));
        @NonNull final UserRegisterRequest request = new UserRegisterRequest();
        request.setLogin(login);
        request.setPassword(password);
        request.setEmail(email);
        getServiceLocator().getUserEndpoint().registerUser(request);
    }

    @Override
    public Role[] getRoles() {
        return null;
    }

}

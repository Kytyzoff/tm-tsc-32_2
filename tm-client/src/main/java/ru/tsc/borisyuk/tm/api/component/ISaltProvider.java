package ru.tsc.borisyuk.tm.api.component;

import lombok.NonNull;

public interface ISaltProvider {

    @NonNull
    String getPasswordSecret();

    @NonNull
    Integer getPasswordIteration();

}

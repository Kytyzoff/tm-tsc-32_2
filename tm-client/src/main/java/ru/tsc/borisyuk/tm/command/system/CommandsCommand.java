package ru.tsc.borisyuk.tm.command.system;

import lombok.NonNull;
import ru.tsc.borisyuk.tm.command.AbstractCommand;

import java.util.Collection;

public final class CommandsCommand extends AbstractSystemCommand {

    @NonNull
    public static final String NAME = "commands";

    @NonNull
    public static final String DESCRIPTION = "Show commands list.";

    @NonNull
    public static final String ARGUMENT = "-cmd";

    @NonNull
    @Override
    public String getName() {
        return NAME;
    }

    @NonNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NonNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        @NonNull final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        commands.stream()
                .map(AbstractCommand::getName)
                .forEach(System.out::println);
    }

}

package ru.tsc.borisyuk.tm.command.server;

import lombok.NonNull;
import ru.tsc.borisyuk.tm.command.AbstractCommand;
import ru.tsc.borisyuk.tm.enumerated.Role;

public class DisconnectCommand extends AbstractCommand {

    @NonNull
    public static final String NAME = "disconnect";

    @NonNull
    public static final String DESCRIPTION = "Disconnect from server";

    public static final String ARGUMENT = null;

    @Override
    public void execute() {
        try {
            getServiceLocator().getConnectionEndpointClient().disconnect();
        } catch (@NonNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
        }
    }

    @Override
    public Role[] getRoles() {
        return null;
    }

    @NonNull
    @Override
    public String getName() {
        return NAME;
    }

    @NonNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

}

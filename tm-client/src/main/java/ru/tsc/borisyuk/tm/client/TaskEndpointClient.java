package ru.tsc.borisyuk.tm.client;

import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.SneakyThrows;
import ru.tsc.borisyuk.tm.api.client.ITaskEndpointClient;
import ru.tsc.borisyuk.tm.dto.request.*;
import ru.tsc.borisyuk.tm.dto.response.*;

@NoArgsConstructor
public final class TaskEndpointClient extends AbstractEndpointClient implements ITaskEndpointClient {

    public TaskEndpointClient(@NonNull final AbstractEndpointClient client) {
        super(client);
    }

    @NonNull
    @Override
    @SneakyThrows
    public TaskChangeStatusByIdResponse taskChangeStatusById(@NonNull final TaskChangeStatusByIdRequest request) {
        return call(request, TaskChangeStatusByIdResponse.class);
    }

    @NonNull
    @Override
    @SneakyThrows
    public TaskChangeStatusByIndexResponse taskChangeStatusByIndex(@NonNull final TaskChangeStatusByIndexRequest request) {
        return call(request, TaskChangeStatusByIndexResponse.class);
    }

    @NonNull
    @Override
    @SneakyThrows
    public TaskRemoveByIdResponse taskRemoveById(@NonNull final TaskRemoveByIdRequest request) {
        return call(request, TaskRemoveByIdResponse.class);
    }

    @NonNull
    @Override
    @SneakyThrows
    public TaskRemoveByIndexResponse taskRemoveByIndex(@NonNull final TaskRemoveByIndexRequest request) {
        return call(request, TaskRemoveByIndexResponse.class);
    }

    @NonNull
    @Override
    @SneakyThrows
    public TaskGetByIdResponse taskGetById(@NonNull final TaskGetByIdRequest request) {
        return call(request, TaskGetByIdResponse.class);
    }

    @NonNull
    @Override
    @SneakyThrows
    public TaskGetByIndexResponse taskGetByIndex(@NonNull final TaskGetByIndexRequest request) {
        return call(request, TaskGetByIndexResponse.class);
    }

    @NonNull
    @Override
    @SneakyThrows
    public TaskUpdateByIdResponse taskUpdateById(@NonNull final TaskUpdateByIdRequest request) {
        return call(request, TaskUpdateByIdResponse.class);
    }

    @NonNull
    @Override
    @SneakyThrows
    public TaskUpdateByIndexResponse taskUpdateByIndex(@NonNull final TaskUpdateByIndexRequest request) {
        return call(request, TaskUpdateByIndexResponse.class);
    }

    @NonNull
    @Override
    @SneakyThrows
    public TaskCreateResponse taskCreate(@NonNull final TaskCreateRequest request) {
        return call(request, TaskCreateResponse.class);
    }

    @NonNull
    @Override
    @SneakyThrows
    public TaskListResponse taskList(@NonNull final TaskListRequest request) {
        return call(request, TaskListResponse.class);
    }

    @NonNull
    @Override
    @SneakyThrows
    public TaskClearResponse taskClear(@NonNull final TaskClearRequest request) {
        return call(request, TaskClearResponse.class);
    }

    @NonNull
    @SneakyThrows
    @Override
    public TaskBindToProjectResponse taskBindToProject(final @NonNull TaskBindToProjectRequest request) {
        return call(request, TaskBindToProjectResponse.class);
    }

    @SneakyThrows
    @Override
    public @NonNull TaskUnbindFromProjectResponse taskUnbindFromProject(final @NonNull TaskUnbindFromProjectRequest request) {
        return call(request, TaskUnbindFromProjectResponse.class);
    }


}

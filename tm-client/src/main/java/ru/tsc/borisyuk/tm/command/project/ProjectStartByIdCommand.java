package ru.tsc.borisyuk.tm.command.project;

import lombok.NonNull;
import ru.tsc.borisyuk.tm.dto.request.ProjectChangeStatusByIdRequest;
import ru.tsc.borisyuk.tm.enumerated.Status;
import ru.tsc.borisyuk.tm.util.TerminalUtil;

public final class ProjectStartByIdCommand extends AbstractProjectCommand {

    @NonNull
    public static final String NAME = "project-start-by-id";

    @NonNull
    public static final String DESCRIPTION = "Start project by id.";

    public static final String ARGUMENT = null;

    @NonNull
    @Override
    public String getName() {
        return NAME;
    }

    @NonNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[START PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @NonNull final String id = TerminalUtil.nextLine();
        @NonNull final Status status = Status.toStatus("IN_PROGRESS");
        @NonNull final ProjectChangeStatusByIdRequest request = new ProjectChangeStatusByIdRequest();
        request.setUserId(id);
        request.setStatus(status);
        getProjectEndpoint().projectChangeStatusById(request);
    }

}

package ru.tsc.borisyuk.tm.command.task;

import lombok.NonNull;
import ru.tsc.borisyuk.tm.dto.request.TaskChangeStatusByIdRequest;
import ru.tsc.borisyuk.tm.enumerated.Status;
import ru.tsc.borisyuk.tm.util.TerminalUtil;

import java.util.Arrays;

public final class TaskChangeStatusByIdCommand extends AbstractTaskCommand {

    @NonNull
    public static final String NAME = "task-change-status-by-id";

    @NonNull
    public static final String DESCRIPTION = "Change task status by id.";

    public static final String ARGUMENT = null;

    @NonNull
    @Override
    public String getName() {
        return NAME;
    }

    @NonNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[CHANGE TASK STATUS BY ID]");
        System.out.println("ENTER ID:");
        @NonNull final String id = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.asList(Status.values()));
        @NonNull final String statusValue = TerminalUtil.nextLine();
        @NonNull final Status status = Status.toStatus(statusValue);
        @NonNull final TaskChangeStatusByIdRequest request = new TaskChangeStatusByIdRequest();
        request.setId(id);
        request.setStatus(status);
        getTaskEndpoint().taskChangeStatusById(request);
    }

}

package ru.tsc.borisyuk.tm.component;

import lombok.NonNull;
import lombok.SneakyThrows;
import org.apache.commons.lang3.StringUtils;
import ru.tsc.borisyuk.tm.api.model.ICommand;
import ru.tsc.borisyuk.tm.command.AbstractCommand;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public final class FileScanner {

    @NonNull
    private final Bootstrap bootstrap;

    @NonNull
    private final ScheduledExecutorService scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();

    @NonNull
    private List<String> commands = new ArrayList<>();

    public FileScanner(@NonNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void start() {
        init();
        scheduledExecutorService.scheduleWithFixedDelay(this::process, 0, 3, TimeUnit.SECONDS);
    }

    private void init() {
        @NonNull final Collection<AbstractCommand> commands = bootstrap.getCommandService().getArgumentCommands();
        this.commands = commands.stream().map(ICommand::getName).collect(Collectors.toList());
    }

    @SneakyThrows
    private void process() {
        for (final String command : commands) {
            if (StringUtils.isEmpty(command)) continue;
            final Path path = Paths.get(command);
            if (Files.isRegularFile(path)) {
                Files.deleteIfExists(path);
                bootstrap.processCommand(command, true);
            }
        }
    }

}

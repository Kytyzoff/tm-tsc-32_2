package ru.tsc.borisyuk.tm.command.user;

import lombok.NonNull;
import ru.tsc.borisyuk.tm.dto.request.UserLockRequest;
import ru.tsc.borisyuk.tm.dto.request.UserLoginRequest;
import ru.tsc.borisyuk.tm.enumerated.Role;
import ru.tsc.borisyuk.tm.util.TerminalUtil;

public final class UserLoginCommand extends AbstractUserCommand {

    @NonNull
    public static final String NAME = "login";

    @NonNull
    public static final String DESCRIPTION = "Login user.";

    public static final String ARGUMENT = null;

    @NonNull
    @Override
    public String getName() {
        return NAME;
    }

    @NonNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[USER LOGIN]");
        System.out.println("ENTER LOGIN:");
        @NonNull final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        @NonNull final String password = TerminalUtil.nextLine();
//        @NonNull final UserLoginRequest request = new UserLoginRequest(login, password);
//        request.setLogin(login);
//        request.setPassword(password);
//        getAuthEndpoint().login(request);
        getServiceLocator().getAuthEndpoint().login(new UserLoginRequest(login, password));
//        getAuthService().login(login, password);
    }

    @Override
    public Role[] getRoles() {
        return null;
    }

}

package ru.tsc.borisyuk.tm.command.project;

import lombok.NonNull;
import ru.tsc.borisyuk.tm.dto.request.ProjectChangeStatusByIndexRequest;
import ru.tsc.borisyuk.tm.enumerated.Status;
import ru.tsc.borisyuk.tm.util.TerminalUtil;

import java.util.Arrays;

public final class ProjectChangeStatusByIndexCommand extends AbstractProjectCommand {

    @NonNull
    public static final String NAME = "project-change-status-by-index";

    @NonNull
    public static final String DESCRIPTION = "Change project status by index.";

    public static final String ARGUMENT = null;

    @NonNull
    @Override
    public String getName() {
        return NAME;
    }

    @NonNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[CHANGE PROJECT STATUS BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NonNull final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.asList(Status.values()));
        @NonNull final String statusValue = TerminalUtil.nextLine();
        @NonNull final Status status = Status.toStatus(statusValue);
        @NonNull final ProjectChangeStatusByIndexRequest request = new ProjectChangeStatusByIndexRequest();
        request.setIndex(index);
        request.setStatus(status);
        getProjectEndpoint().projectChangeStatusByIndex(request);
    }

}

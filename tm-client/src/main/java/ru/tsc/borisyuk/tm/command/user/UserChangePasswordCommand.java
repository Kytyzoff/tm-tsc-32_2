package ru.tsc.borisyuk.tm.command.user;

import lombok.NonNull;
import ru.tsc.borisyuk.tm.dto.request.UserChangePasswordRequest;
import ru.tsc.borisyuk.tm.enumerated.Role;
import ru.tsc.borisyuk.tm.util.TerminalUtil;

public final class UserChangePasswordCommand extends AbstractUserCommand {

    @NonNull
    public static final String NAME = "user-change-password";

    @NonNull
    public static final String DESCRIPTION = "Change user password.";

    public static final String ARGUMENT = null;

    @NonNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NonNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[CHANGE USER PASSWORD]");
        System.out.println("ENTER NEW PASSWORD:");
        @NonNull final String password = TerminalUtil.nextLine();
        @NonNull final UserChangePasswordRequest request = new UserChangePasswordRequest();
        request.setPassword(password);
        getUserEndpoint().changeUserPassword(request);
    }

    @NonNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}

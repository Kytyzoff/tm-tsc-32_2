package ru.tsc.borisyuk.tm.command.project;

import lombok.NonNull;
import ru.tsc.borisyuk.tm.dto.request.ProjectChangeStatusByIndexRequest;
import ru.tsc.borisyuk.tm.enumerated.Status;
import ru.tsc.borisyuk.tm.util.TerminalUtil;

public final class ProjectStartByIndexCommand extends AbstractProjectCommand {

    @NonNull
    public static final String NAME = "project-start-by-index";

    @NonNull
    public static final String DESCRIPTION = "Start project by index.";

    public static final String ARGUMENT = null;

    @NonNull
    @Override
    public String getName() {
        return NAME;
    }

    @NonNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[START PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NonNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NonNull final Status status = Status.toStatus("COMPLETED");
        @NonNull final ProjectChangeStatusByIndexRequest request = new ProjectChangeStatusByIndexRequest();
        request.setIndex(index);
        request.setStatus(status);
        getProjectEndpoint().projectChangeStatusByIndex(request);
    }

}

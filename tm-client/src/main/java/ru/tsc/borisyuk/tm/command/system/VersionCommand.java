package ru.tsc.borisyuk.tm.command.system;

import lombok.NonNull;

public final class VersionCommand extends AbstractSystemCommand {

    @NonNull
    public static final String NAME = "version";

    @NonNull
    public static final String DESCRIPTION = "Show application version.";

    @NonNull
    public static final String ARGUMENT = "-v";

    @NonNull
    @Override
    public String getName() {
        return NAME;
    }

    @NonNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NonNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println(getPropertyService().getApplicationVersion());
    }

}

package ru.tsc.borisyuk.tm.command.user;

import lombok.NonNull;
import ru.tsc.borisyuk.tm.client.AuthEndpointClient;
import ru.tsc.borisyuk.tm.client.UserEndpointClient;
import ru.tsc.borisyuk.tm.command.AbstractCommand;

public abstract class AbstractUserCommand extends AbstractCommand {
    @NonNull
    protected UserEndpointClient getUserEndpoint() {
        return serviceLocator.getUserEndpoint();
    }

    @NonNull
    protected AuthEndpointClient getAuthEndpoint() {
        return serviceLocator.getAuthEndpoint();
    }

}

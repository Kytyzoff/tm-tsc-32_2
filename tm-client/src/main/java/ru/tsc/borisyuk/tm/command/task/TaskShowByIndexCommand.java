package ru.tsc.borisyuk.tm.command.task;

import lombok.NonNull;
import ru.tsc.borisyuk.tm.dto.request.TaskGetByIdRequest;
import ru.tsc.borisyuk.tm.dto.request.TaskGetByIndexRequest;
import ru.tsc.borisyuk.tm.model.Task;
import ru.tsc.borisyuk.tm.util.TerminalUtil;

public final class TaskShowByIndexCommand extends AbstractTaskCommand {

    @NonNull
    public static final String NAME = "task-show-by-index";

    @NonNull
    public static final String DESCRIPTION = "Show task by index.";

    public static final String ARGUMENT = null;

    @NonNull
    @Override
    public String getName() {
        return NAME;
    }

    @NonNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NonNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NonNull final TaskGetByIndexRequest request = new TaskGetByIndexRequest();
        request.setIndex(index);
        final Task task = getTaskEndpoint().taskGetByIndex(request).getTask();
        showTask(task);
    }

}

package ru.tsc.borisyuk.tm.exception.system;

import ru.tsc.borisyuk.tm.exception.AbstractException;

public final class ArgumentNotSupportedException extends AbstractException {

    public ArgumentNotSupportedException() {
        super("Error! Argument is not supported...");
    }

    public ArgumentNotSupportedException(final String message) {
        super("Error! Argument ``" + message + "`` is not supported...");
    }

}

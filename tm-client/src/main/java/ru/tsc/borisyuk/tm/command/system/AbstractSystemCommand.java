package ru.tsc.borisyuk.tm.command.system;

import lombok.NonNull;
import ru.tsc.borisyuk.tm.api.service.ICommandService;
import ru.tsc.borisyuk.tm.api.service.IPropertyService;
import ru.tsc.borisyuk.tm.command.AbstractCommand;
import ru.tsc.borisyuk.tm.enumerated.Role;

public abstract class AbstractSystemCommand extends AbstractCommand {

    @NonNull
    protected ICommandService getCommandService() {
        return serviceLocator.getCommandService();
    }

    @NonNull
    protected IPropertyService getPropertyService() {
        return serviceLocator.getPropertyService();
    }

    @Override
    public Role[] getRoles() {
        return null;
    }

}

package ru.tsc.borisyuk.tm.command.user;

import lombok.NonNull;
import ru.tsc.borisyuk.tm.dto.request.UserRemoveRequest;
import ru.tsc.borisyuk.tm.enumerated.Role;
import ru.tsc.borisyuk.tm.util.TerminalUtil;

public final class UserRemoveCommand extends AbstractUserCommand {

    @NonNull
    public static final String NAME = "user-remove";

    @NonNull
    public static final String DESCRIPTION = "Remove user.";

    public static final String ARGUMENT = null;

    @NonNull
    @Override
    public String getName() {
        return NAME;
    }

    @NonNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE USER]");
        System.out.println("ENTER LOGIN:");
        @NonNull final String login = TerminalUtil.nextLine();
        @NonNull final UserRemoveRequest request = new UserRemoveRequest();
        request.setLogin(login);
        getUserEndpoint().removeUser(request);
    }

    @NonNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}

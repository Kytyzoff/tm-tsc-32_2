package ru.tsc.borisyuk.tm.api.repository;

import lombok.NonNull;
import ru.tsc.borisyuk.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandRepository {

    @NonNull
    Collection<AbstractCommand> getTerminalCommands();

    @NonNull
    Collection<AbstractCommand> getArgumentCommands();

    void add(AbstractCommand command);

    AbstractCommand getCommandByName(String name);

    AbstractCommand getCommandByArgument(String argument);

}

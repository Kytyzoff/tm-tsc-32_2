package ru.tsc.borisyuk.tm.command.binding;

import lombok.NonNull;
import ru.tsc.borisyuk.tm.client.TaskEndpointClient;
import ru.tsc.borisyuk.tm.command.AbstractCommand;
import ru.tsc.borisyuk.tm.enumerated.Role;

public abstract class AbstractBindingCommand extends AbstractCommand {
    @NonNull
    protected TaskEndpointClient getTaskEndpoint() {
        return serviceLocator.getTaskEndpoint();
    }

    @NonNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}

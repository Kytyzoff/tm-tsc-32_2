package ru.tsc.borisyuk.tm.command.project;

import com.sun.istack.internal.Nullable;
import lombok.NonNull;
import ru.tsc.borisyuk.tm.dto.request.ProjectChangeStatusByIdRequest;
import ru.tsc.borisyuk.tm.dto.request.ProjectChangeStatusByIndexRequest;
import ru.tsc.borisyuk.tm.enumerated.Status;
import ru.tsc.borisyuk.tm.model.Project;
import ru.tsc.borisyuk.tm.util.TerminalUtil;

import java.util.Arrays;

public final class ProjectChangeStatusByIdCommand extends AbstractProjectCommand {

    @NonNull
    public static final String NAME = "project-change-status-by-id";

    @NonNull
    public static final String DESCRIPTION = "Change project status by id.";

    public static final String ARGUMENT = null;

    @NonNull
    @Override
    public String getName() {
        return NAME;
    }

    @NonNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[CHANGE PROJECT STATUS BY ID]");
        System.out.println("ENTER ID:");
        @NonNull final String id = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.asList(Status.values()));
        @NonNull final String statusValue = TerminalUtil.nextLine();
        @NonNull final Status status = Status.toStatus(statusValue);
        @NonNull final ProjectChangeStatusByIdRequest request = new ProjectChangeStatusByIdRequest();
        request.setUserId(id);
        request.setStatus(status);
        getProjectEndpoint().projectChangeStatusById(request);
    }

}

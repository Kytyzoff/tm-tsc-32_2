package ru.tsc.borisyuk.tm.command.task;

import lombok.NonNull;
import ru.tsc.borisyuk.tm.client.TaskEndpointClient;
import ru.tsc.borisyuk.tm.dto.request.TaskListRequest;
import ru.tsc.borisyuk.tm.dto.response.TaskListResponse;
import ru.tsc.borisyuk.tm.enumerated.Sort;
import ru.tsc.borisyuk.tm.model.Task;
import ru.tsc.borisyuk.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class TaskListCommand extends AbstractTaskCommand {

    @NonNull
    public static final String NAME = "task-list";

    @NonNull
    public static final String DESCRIPTION = "Show task list.";

    public static final String ARGUMENT = null;

    @NonNull
    @Override
    public String getName() {
        return NAME;
    }

    @NonNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[TASK LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.asList(Sort.values()));
        @NonNull final String sortValue = TerminalUtil.nextLine();
        final Sort sort = Sort.toSort(sortValue);
        @NonNull final TaskEndpointClient taskEndpoint = getServiceLocator().getTaskEndpoint();
        final TaskListRequest request = new TaskListRequest();
        request.setSort(sort);
        @NonNull final TaskListResponse taskListResponse = taskEndpoint.taskList(request);
        final List<Task> tasks = taskListResponse.getTasks();
        int index = 1;
        for (final Task task : tasks) {
            if (task == null) continue;
            System.out.println(index + ". " + task);
            index++;
        }
    }

}

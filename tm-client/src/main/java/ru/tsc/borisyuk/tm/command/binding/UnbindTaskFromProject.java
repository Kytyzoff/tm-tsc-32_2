package ru.tsc.borisyuk.tm.command.binding;

import lombok.NonNull;
import ru.tsc.borisyuk.tm.dto.request.TaskUnbindFromProjectRequest;
import ru.tsc.borisyuk.tm.util.TerminalUtil;

public final class UnbindTaskFromProject extends AbstractBindingCommand {

    @NonNull
    public static final String NAME = "task-unbind-from-project";

    @NonNull
    public static final String DESCRIPTION = "Unbind task from project.";

    public static final String ARGUMENT = null;

    @NonNull
    @Override
    public String getName() {
        return NAME;
    }

    @NonNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[BIND TASK FROM PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        @NonNull final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID:");
        @NonNull final String taskId = TerminalUtil.nextLine();
        @NonNull final TaskUnbindFromProjectRequest request = new TaskUnbindFromProjectRequest();
        request.setProjectId(projectId);
        request.setTaskId(taskId);
        getTaskEndpoint().taskUnbindFromProject(request);
    }

}

package ru.tsc.borisyuk.tm.client;

import lombok.NoArgsConstructor;
import lombok.NonNull;
import ru.tsc.borisyuk.tm.api.client.IUserEndpointClient;
import ru.tsc.borisyuk.tm.dto.request.*;
import ru.tsc.borisyuk.tm.dto.response.*;

@NoArgsConstructor
public final class UserEndpointClient extends AbstractEndpointClient implements IUserEndpointClient {

    public UserEndpointClient( @NonNull final AbstractEndpointClient client) {
        super(client);
    }

    @NonNull
    @Override
    public UserLockResponse lockUser(@NonNull final UserLockRequest request) {
        return call(request, UserLockResponse.class);
    }

    @NonNull
    @Override
    public UserUnlockResponse unlockUser(@NonNull final UserUnlockRequest request) {
        return call(request, UserUnlockResponse.class);
    }

    @NonNull
    @Override
    public UserChangePasswordResponse changeUserPassword(@NonNull final UserChangePasswordRequest request) {
        return call(request, UserChangePasswordResponse.class);
    }

    @NonNull
    @Override
    public UserRegisterResponse registerUser(@NonNull final UserRegisterRequest request) {
        return call(request, UserRegisterResponse.class);
    }

    @NonNull
    @Override
    public UserRemoveResponse removeUser(@NonNull final UserRemoveRequest request) {
        return call(request, UserRemoveResponse.class);
    }

    @NonNull
    @Override
    public UserUpdateProfileResponse updateUserProfile(@NonNull final UserUpdateProfileRequest request) {
        return call(request, UserUpdateProfileResponse.class);
    }

}
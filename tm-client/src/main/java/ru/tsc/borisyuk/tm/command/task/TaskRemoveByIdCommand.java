package ru.tsc.borisyuk.tm.command.task;

import lombok.NonNull;
import ru.tsc.borisyuk.tm.dto.request.ProjectRemoveByIdRequest;
import ru.tsc.borisyuk.tm.dto.request.TaskRemoveByIdRequest;
import ru.tsc.borisyuk.tm.util.TerminalUtil;

public final class TaskRemoveByIdCommand extends AbstractTaskCommand {

    @NonNull
    public static final String NAME = "task-remove-by-id";

    @NonNull
    public static final String DESCRIPTION = "Remove task by id.";

    public static final String ARGUMENT = null;

    @NonNull
    @Override
    public String getName() {
        return NAME;
    }

    @NonNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK BY ID]");
        System.out.println("ENTER ID:");
        @NonNull final String id = TerminalUtil.nextLine();
        @NonNull final TaskRemoveByIdRequest request = new TaskRemoveByIdRequest();
        request.setUserId(id);
        getTaskEndpoint().taskRemoveById(request);
    }

}

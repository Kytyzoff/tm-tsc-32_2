package ru.tsc.borisyuk.tm.command.project;

import lombok.NonNull;
import ru.tsc.borisyuk.tm.dto.request.ProjectChangeStatusByIdRequest;
import ru.tsc.borisyuk.tm.dto.request.ProjectUpdateByIdRequest;
import ru.tsc.borisyuk.tm.util.TerminalUtil;

public final class ProjectUpdateByIdCommand extends AbstractProjectCommand {

    @NonNull
    public static final String NAME = "project-update-by-id";

    @NonNull
    public static final String DESCRIPTION = "Update project by id.";

    public static final String ARGUMENT = null;

    @NonNull
    @Override
    public String getName() {
        return NAME;
    }

    @NonNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @NonNull final String id = TerminalUtil.nextLine();
        System.out.println("ENTER NAME:");
        @NonNull final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @NonNull final String description = TerminalUtil.nextLine();
        @NonNull final ProjectUpdateByIdRequest request = new ProjectUpdateByIdRequest();
        request.setUserId(id);
        request.setName(name);
        request.setDescription(description);
        getProjectEndpoint().projectUpdateById(request);
    }

}

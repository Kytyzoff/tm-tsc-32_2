package ru.tsc.borisyuk.tm.client;

import lombok.NoArgsConstructor;
import lombok.NonNull;
import ru.tsc.borisyuk.tm.api.client.ISystemEndpointClient;
import ru.tsc.borisyuk.tm.dto.request.ServerAboutRequest;
import ru.tsc.borisyuk.tm.dto.request.ServerVersionRequest;
import ru.tsc.borisyuk.tm.dto.response.ServerAboutResponse;
import ru.tsc.borisyuk.tm.dto.response.ServerVersionResponse;

@NoArgsConstructor
public final class SystemEndpointClient extends AbstractEndpointClient implements ISystemEndpointClient {

    public static void main(String[] args) {
        SystemEndpointClient client = new SystemEndpointClient();
        client.connect();
        @NonNull final ServerAboutResponse about = client.getAbout(new ServerAboutRequest());
        System.out.println(about.getEmail());
        System.out.println(about.getName());
        System.out.println("---");
        @NonNull final ServerVersionResponse version = client.getVersion(new ServerVersionRequest());
        System.out.println(version.getVersion());
        client.disconnect();
    }

    @NonNull
    @Override
    public ServerAboutResponse getAbout(@NonNull final ServerAboutRequest request) {
        return (ServerAboutResponse) call(request, ServerAboutResponse.class);
    }

    @NonNull
    @Override
    public ServerVersionResponse getVersion(@NonNull final ServerVersionRequest request) {
        return (ServerVersionResponse) call(request, ServerVersionResponse.class);
    }

}

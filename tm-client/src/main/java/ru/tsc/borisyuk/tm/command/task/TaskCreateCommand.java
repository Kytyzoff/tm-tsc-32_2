package ru.tsc.borisyuk.tm.command.task;

import lombok.NonNull;
import ru.tsc.borisyuk.tm.dto.request.TaskChangeStatusByIndexRequest;
import ru.tsc.borisyuk.tm.dto.request.TaskCreateRequest;
import ru.tsc.borisyuk.tm.util.TerminalUtil;

import java.util.Date;

public final class TaskCreateCommand extends AbstractTaskCommand {

    @NonNull
    public static final String NAME = "task-create";

    @NonNull
    public static final String DESCRIPTION = "Create task.";

    public static final String ARGUMENT = null;

    @NonNull
    @Override
    public String getName() {
        return NAME;
    }

    @NonNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER NAME:");
        @NonNull final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @NonNull final String description = TerminalUtil.nextLine();
//        System.out.println("ENTER DATE BEGIN:");
//        final Date dateBegin = TerminalUtil.nextDate();
//        System.out.println("ENTER DATE END:");
//        final Date dateEnd = TerminalUtil.nextDate();
        @NonNull final TaskCreateRequest request = new TaskCreateRequest();
        request.setName(name);
        request.setDescription(description);
        getTaskEndpoint().taskCreate(request);
    }

}

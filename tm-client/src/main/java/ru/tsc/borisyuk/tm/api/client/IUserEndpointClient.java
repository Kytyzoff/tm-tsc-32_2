package ru.tsc.borisyuk.tm.api.client;

import ru.tsc.borisyuk.tm.api.endpoint.IUserEndpoint;

public interface IUserEndpointClient extends IUserEndpoint, IEndpointClient {

}
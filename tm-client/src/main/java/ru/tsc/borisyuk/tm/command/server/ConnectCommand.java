package ru.tsc.borisyuk.tm.command.server;

import lombok.NonNull;
import ru.tsc.borisyuk.tm.api.client.IEndpointClient;
import ru.tsc.borisyuk.tm.api.service.IServiceLocator;
import ru.tsc.borisyuk.tm.command.AbstractCommand;
import ru.tsc.borisyuk.tm.enumerated.Role;

import java.net.Socket;

public class ConnectCommand extends AbstractCommand {

    @NonNull
    public static final String NAME = "connect";

    @NonNull
    public static final String DESCRIPTION = "Connect to server";

    public static final String ARGUMENT = null;

    @NonNull
    @Override
    public String getName() {
        return NAME;
    }

    @NonNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        try {
            @NonNull final IServiceLocator serviceLocator = getServiceLocator();
            @NonNull final IEndpointClient endpointClient = serviceLocator.getAuthEndpoint();
            endpointClient.connect();
            final Socket socket = endpointClient.getSocket();
            serviceLocator.getProjectEndpoint().setSocket(socket);
            serviceLocator.getAuthEndpointClient().setSocket(socket);
            serviceLocator.getDomainEndpointClient().setSocket(socket);
            serviceLocator.getProjectEndpointClient().setSocket(socket);
            serviceLocator.getTaskEndpointClient().setSocket(socket);
            serviceLocator.getSystemEndpointClient().setSocket(socket);
            serviceLocator.getUserEndpointClient().setSocket(socket);
        } catch (@NonNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
        }
    }

    @Override
    public Role[] getRoles() {
        return null;
    }

}

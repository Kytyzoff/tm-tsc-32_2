package ru.tsc.borisyuk.tm.component;

import lombok.Getter;
import lombok.NonNull;
import lombok.SneakyThrows;
import org.reflections.Reflections;
import ru.tsc.borisyuk.tm.api.client.*;
import ru.tsc.borisyuk.tm.client.ConnectionEndpointClient;
import ru.tsc.borisyuk.tm.command.server.ConnectCommand;
import ru.tsc.borisyuk.tm.command.server.DisconnectCommand;
import ru.tsc.borisyuk.tm.exception.system.ArgumentNotSupportedException;
import ru.tsc.borisyuk.tm.exception.system.CommandNotSupportedException;
import ru.tsc.borisyuk.tm.repository.CommandRepository;
import ru.tsc.borisyuk.tm.api.repository.ICommandRepository;
import ru.tsc.borisyuk.tm.api.service.ICommandService;
import ru.tsc.borisyuk.tm.api.service.ILoggerService;
import ru.tsc.borisyuk.tm.api.service.IPropertyService;
import ru.tsc.borisyuk.tm.api.service.IServiceLocator;
import ru.tsc.borisyuk.tm.client.*;
import ru.tsc.borisyuk.tm.command.AbstractCommand;
import ru.tsc.borisyuk.tm.service.CommandService;
import ru.tsc.borisyuk.tm.service.LoggerService;
import ru.tsc.borisyuk.tm.service.PropertyService;
import ru.tsc.borisyuk.tm.util.SystemUtil;
import ru.tsc.borisyuk.tm.util.TerminalUtil;

import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;

public class Bootstrap implements IServiceLocator {

    @NonNull
    private static final String PACKAGE_COMMANDS = "ru.tsc.borisyuk.tm.command";

    @NonNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @Getter
    @NonNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @Getter
    @NonNull
    private final ILoggerService loggerService = new LoggerService();

    @Getter
    @NonNull
    private final IPropertyService propertyService = new PropertyService();

    @NonNull
    private final FileScanner fileScanner = new FileScanner(this);

    @Getter
    @NonNull
    private final AuthEndpointClient authEndpoint = new AuthEndpointClient();

    @Getter
    @NonNull
    private final ProjectEndpointClient projectEndpoint = new ProjectEndpointClient();

    @Getter
    @NonNull
    private final TaskEndpointClient taskEndpoint = new TaskEndpointClient();

    @Getter
    @NonNull
    private final UserEndpointClient userEndpoint = new UserEndpointClient();

    @Getter
    @NonNull
    private final IEndpointClient connectionEndpointClient = new ConnectionEndpointClient();

    @Getter
    @NonNull
    private final IAuthEndpointClient authEndpointClient = new AuthEndpointClient();

    @Getter
    @NonNull
    private final IDomainEndpointClient domainEndpointClient = new DomainEndpointClient();

    @Getter
    @NonNull
    private final ProjectEndpointClient projectEndpointClient = new ProjectEndpointClient();

    @Getter
    @NonNull
    private final ITaskEndpointClient taskEndpointClient = new TaskEndpointClient();

    @Getter
    @NonNull
    private final ISystemEndpointClient systemEndpointClient = new SystemEndpointClient();

    @Getter
    @NonNull
    private final IUserEndpointClient userEndpointClient = new UserEndpointClient();

    {
        @NonNull final Reflections reflections = new Reflections(PACKAGE_COMMANDS);
        @NonNull final Set<Class<? extends AbstractCommand>> classes =
                reflections.getSubTypesOf(AbstractCommand.class);
        for (@NonNull final Class<? extends AbstractCommand> clazz : classes) {
            registerCommand(clazz);
        }
    }

    private void registerCommand(final AbstractCommand command) {
        if (command == null) return;
        command.setServiceLocator(this);
        commandService.add(command);
    }

    @SneakyThrows
    private void registerCommand(@NonNull final Class<? extends AbstractCommand> clazz) {
        if (Modifier.isAbstract(clazz.getModifiers())) return;
        if (!AbstractCommand.class.isAssignableFrom(clazz)) return;
        @NonNull final AbstractCommand command = (AbstractCommand) clazz.newInstance();
        registerCommand(command);
    }

    public void run(final String[] args) {
        if (processArgument(args)) System.exit(0);
        prepareStartup();
        processCommand();
    }

    private void processCommand() {
        while (true) {
            try {
                System.out.println("\nENTER COMMAND:");
                @NonNull final String command = TerminalUtil.nextLine();
                processCommand(command, true);
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (@NonNull final Exception e) {
                System.err.println("[FAIL]");
                loggerService.error(e);
            }
        }
    }

    private void prepareStartup() {
        loggerService.info("Welcome to Task Manager");
        initPID();
        fileScanner.start();
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        connect();
    }

    private void prepareShutdown() {
        loggerService.info("Task Manager is shutting down...");
        disconnect();
    }

    @SneakyThrows
    private void initPID() {
        @NonNull final String filename = "task-manager.pid";
        @NonNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NonNull final File file = new File(filename);
        file.deleteOnExit();
    }

    protected void processCommand(final String command, final boolean checkRoles) {
        final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        if (checkRoles)
            processCommand(abstractCommand);
        else
            abstractCommand.execute();
    }

    protected void processCommand(@NonNull final AbstractCommand abstractCommand) {
//        authService.checkRoles(abstractCommand.getRoles());
        abstractCommand.execute();
    }

    private boolean processArgument(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        processArgument(arg);
        return true;
    }

    private void processArgument(final String argument) {
        final AbstractCommand abstractCommand = commandService.getCommandByArgument(argument);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(argument);
        abstractCommand.execute();
    }

    private void connect() {
        processCommand(commandService.getCommandByName(ConnectCommand.NAME));
    }

    private void disconnect() {
        processCommand(commandService.getCommandByName(DisconnectCommand.NAME));
    }


}

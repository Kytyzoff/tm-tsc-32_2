package ru.tsc.borisyuk.tm.api.client;

import ru.tsc.borisyuk.tm.api.endpoint.IProjectEndpoint;

public interface IProjectEndpointClient extends IProjectEndpoint, IEndpointClient {

}
package ru.tsc.borisyuk.tm.repository;

import lombok.NonNull;
import org.apache.commons.lang3.StringUtils;
import ru.tsc.borisyuk.tm.api.repository.ICommandRepository;
import ru.tsc.borisyuk.tm.command.AbstractCommand;

import java.util.Collection;
import java.util.Map;
import java.util.TreeMap;

public class CommandRepository implements ICommandRepository {

    @NonNull
    private final Map<String, AbstractCommand> mapByName = new TreeMap<>();

    @NonNull
    private final Map<String, AbstractCommand> mapByArgument = new TreeMap<>();

    @NonNull
    @Override
    public Collection<AbstractCommand> getTerminalCommands() {
        return mapByName.values();
    }

    @NonNull
    @Override
    public Collection<AbstractCommand> getArgumentCommands() {
        return mapByArgument.values();
    }

    @Override
    public void add(AbstractCommand command) {
        final String name = command.getName();
        if (StringUtils.isNotEmpty(name)) mapByName.put(name, command);
        final String argument = command.getArgument();
        if (StringUtils.isNotEmpty(argument)) mapByArgument.put(argument, command);
    }

    @Override
    public AbstractCommand getCommandByName(final String name) {
        return mapByName.get(name);
    }

    @Override
    public AbstractCommand getCommandByArgument(final String argument) {
        return mapByArgument.get(argument);
    }

}

package ru.tsc.borisyuk.tm.command.binding;

import lombok.NonNull;
import ru.tsc.borisyuk.tm.command.task.AbstractTaskCommand;
import ru.tsc.borisyuk.tm.dto.request.TaskBindToProjectRequest;
import ru.tsc.borisyuk.tm.util.TerminalUtil;

public final class BindTaskToProject extends AbstractTaskCommand {

    @NonNull
    public static final String NAME = "task-bind-to-project";

    @NonNull
    public static final String DESCRIPTION = "Bind task to project.";

    public static final String ARGUMENT = null;

    @NonNull
    @Override
    public String getName() {
        return NAME;
    }

    @NonNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[BIND TASK TO PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        @NonNull final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID:");
        @NonNull final String taskId = TerminalUtil.nextLine();
        @NonNull final TaskBindToProjectRequest request = new TaskBindToProjectRequest();
        request.setProjectId(projectId);
        request.setTaskId(taskId);
        getTaskEndpoint().taskBindToProject(request);
    }

}

package ru.tsc.borisyuk.tm.util;

import lombok.NonNull;

public interface SystemUtil {

    @NonNull
    static Long getPID() {
        final String processName = java.lang.management.ManagementFactory.getRuntimeMXBean().getName();
        if (processName != null && processName.length() > 0) {
            try {
                return Long.parseLong(processName.split("@")[0]);
            } catch (@NonNull final Exception e) {
                return 0L;
            }
        }
        return 0L;
    }

}

package ru.tsc.borisyuk.tm.api.service;

import lombok.NonNull;
import ru.tsc.borisyuk.tm.api.client.*;
import ru.tsc.borisyuk.tm.client.AuthEndpointClient;
import ru.tsc.borisyuk.tm.client.ProjectEndpointClient;
import ru.tsc.borisyuk.tm.client.TaskEndpointClient;
import ru.tsc.borisyuk.tm.client.UserEndpointClient;

public interface IServiceLocator {

    @NonNull
    ICommandService getCommandService();

    @NonNull
    ILoggerService getLoggerService();

    @NonNull
    IPropertyService getPropertyService();

    @NonNull
    AuthEndpointClient getAuthEndpoint();

    @NonNull
    ProjectEndpointClient getProjectEndpoint();

    @NonNull
    TaskEndpointClient getTaskEndpoint();

    @NonNull
    UserEndpointClient getUserEndpoint();

    @NonNull
    IEndpointClient getConnectionEndpointClient();

    @NonNull
    IAuthEndpointClient getAuthEndpointClient();

    @NonNull
    IDomainEndpointClient getDomainEndpointClient();

    @NonNull
    IProjectEndpointClient getProjectEndpointClient();

    @NonNull
    ITaskEndpointClient getTaskEndpointClient();

    @NonNull
    ISystemEndpointClient getSystemEndpointClient();

    @NonNull
    IUserEndpointClient getUserEndpointClient();

}
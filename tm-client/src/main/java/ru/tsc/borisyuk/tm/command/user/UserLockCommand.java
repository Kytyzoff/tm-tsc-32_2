package ru.tsc.borisyuk.tm.command.user;

import lombok.NonNull;
import ru.tsc.borisyuk.tm.dto.request.UserLockRequest;
import ru.tsc.borisyuk.tm.enumerated.Role;
import ru.tsc.borisyuk.tm.util.TerminalUtil;

public final class UserLockCommand extends AbstractUserCommand {

    @NonNull
    public static final String NAME = "user-lock";

    @NonNull
    public static final String DESCRIPTION = "Lock user.";

    public static final String ARGUMENT = null;

    @NonNull
    @Override
    public String getName() {
        return NAME;
    }

    @NonNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[LOCK USER]");
        System.out.println("ENTER LOGIN:");
        @NonNull final String login = TerminalUtil.nextLine();
        @NonNull final UserLockRequest request = new UserLockRequest();
        request.setLogin(login);
        getUserEndpoint().lockUser(request);
    }

    @NonNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}

package ru.tsc.borisyuk.tm.service;

import com.jcabi.manifests.Manifests;
import lombok.NonNull;
import lombok.SneakyThrows;
import ru.tsc.borisyuk.tm.api.service.IPropertyService;

import java.util.Properties;

public class PropertyService implements IPropertyService {

    @NonNull
    public static final String FILE_NAME = "application.properties";

    @NonNull
    private static final String APPLICATION_VERSION_KEY = "buildNumber";

    @NonNull
    private static final String AUTHOR_NAME_KEY = "developer";

    @NonNull
    private static final String AUTHOR_EMAIL_KEY = "email";

    @NonNull
    private static final String PASSWORD_SECRET_DEFAULT = "65767676545";

    @NonNull
    private static final String PASSWORD_SECRET_KEY = "password.secret";

    @NonNull
    private static final String PASSWORD_ITERATION_DEFAULT = "34543";

    @NonNull
    private static final String PASSWORD_ITERATION_KEY = "password.iteration";

    @NonNull
    private static final String DATA_FILE_BINARY = "data.file.binary";

    @NonNull
    private static final String DATA_FILE_BINARY_DEFAULT = "data.bin";

    @NonNull
    private static final String DATA_FILE_BASE64 = "data.file.base64";

    @NonNull
    private static final String DATA_FILE_BASE64_DEFAULT = "data.base64";

    @NonNull
    private static final String DATA_FILE_JAXB_XML = "data.file.jaxb.xml";

    @NonNull
    private static final String DATA_FILE_JAXB_XML_DEFAULT = "data.jaxb.xml";

    @NonNull
    private static final String DATA_FILE_JAXB_JSON = "data.file.jaxb.json";

    @NonNull
    private static final String DATA_FILE_JAXB_JSON_DEFAULT = "data.jaxb.json";

    @NonNull
    private static final String DATA_FILE_FASTER_XML = "data.file.faster.xml";

    @NonNull
    private static final String DATA_FILE_FASTER_XML_DEFAULT = "data.faster.xml";

    @NonNull
    private static final String DATA_FILE_FASTER_JSON = "data.file.faster.json";

    @NonNull
    private static final String DATA_FILE_FASTER_JSON_DEFAULT = "data.faster.json";

    @NonNull
    private static final String DATA_FILE_FASTER_YAML = "data.file.faster.yml";

    @NonNull
    private static final String DATA_FILE_FASTER_YAML_DEFAULT = "data.faster.yml";

    @NonNull
    private static final String DATA_FILE_BACKUP_XML = "data.backup.xml";

    @NonNull
    private static final String DATA_FILE_BACKUP_XML_DEFAULT = "data.backup.xml";

    @NonNull
    private static final String SERVER_PORT = "server.port";

    @NonNull
    private static final String SERVER_PORT_DEFAULT = "6060";

    @NonNull
    private static final String EMPTY_VALUE = "---";

    @NonNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        properties.load(ClassLoader.getSystemResourceAsStream(FILE_NAME));
    }

    @NonNull
    private String getStringValue(@NonNull final String key) {
        return getStringValue(key, EMPTY_VALUE);
    }

    @NonNull
    private String getStringValue(@NonNull final String key, @NonNull final String defaultValue) {
        @NonNull final String envKey = getEnvKey(key);
        if (System.getProperties().containsKey(envKey)) return System.getProperties().getProperty(envKey);
        if (System.getenv().containsKey(envKey)) return System.getenv(envKey);
        return properties.getProperty(key, defaultValue);
    }

    @NonNull
    private String getEnvKey(@NonNull final String key) {
        return key.replace(".", "_").toUpperCase();
    }

    @NonNull
    @Override
    public String getApplicationVersion() {
        return Manifests.read(APPLICATION_VERSION_KEY);
    }

    @NonNull
    @Override
    public String getAuthorName() {
        return Manifests.read(AUTHOR_NAME_KEY);
    }

    @NonNull
    @Override
    public String getAuthorEmail() {
        return Manifests.read(AUTHOR_EMAIL_KEY);
    }

    @NonNull
    @Override
    public String getPasswordSecret() {
        return getStringValue(PASSWORD_SECRET_KEY, PASSWORD_SECRET_DEFAULT);
    }

    @NonNull
    @Override
    public Integer getPasswordIteration() {
        @NonNull final String value = getStringValue(PASSWORD_ITERATION_KEY, PASSWORD_ITERATION_DEFAULT);
        return Integer.parseInt(value);
    }

    @NonNull
    @Override
    public String getDataFileBinary() {
        return getStringValue(DATA_FILE_BINARY, DATA_FILE_BINARY_DEFAULT);
    }

    @NonNull
    @Override
    public String getDataFileBase64() {
        return getStringValue(DATA_FILE_BASE64, DATA_FILE_BASE64_DEFAULT);
    }

    @NonNull
    @Override
    public String getDataFileJaxbXml() {
        return getStringValue(DATA_FILE_JAXB_XML, DATA_FILE_JAXB_XML_DEFAULT);
    }

    @NonNull
    @Override
    public String getDataFileJaxbJson() {
        return getStringValue(DATA_FILE_JAXB_JSON, DATA_FILE_JAXB_JSON_DEFAULT);
    }

    @NonNull
    @Override
    public String getDataFileFasterXml() {
        return getStringValue(DATA_FILE_FASTER_XML, DATA_FILE_FASTER_XML_DEFAULT);
    }

    @NonNull
    @Override
    public String getDataFileFasterJson() {
        return getStringValue(DATA_FILE_FASTER_JSON, DATA_FILE_FASTER_JSON_DEFAULT);
    }

    @NonNull
    @Override
    public String getDataFileFasterYaml() {
        return getStringValue(DATA_FILE_FASTER_YAML, DATA_FILE_FASTER_YAML_DEFAULT);
    }

    @NonNull
    @Override
    public String getDataFileBackupXml() {
        return getStringValue(DATA_FILE_BACKUP_XML, DATA_FILE_BACKUP_XML_DEFAULT);
    }

    @Override
    public @NonNull Integer getServerPort() {
        @NonNull final String value = getStringValue(SERVER_PORT, SERVER_PORT_DEFAULT);
        return Integer.parseInt(value);
    }

}

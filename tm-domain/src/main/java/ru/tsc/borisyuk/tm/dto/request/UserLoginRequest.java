package ru.tsc.borisyuk.tm.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class UserLoginRequest extends AbstractUserRequest {

    private String login;

    private String password;

}

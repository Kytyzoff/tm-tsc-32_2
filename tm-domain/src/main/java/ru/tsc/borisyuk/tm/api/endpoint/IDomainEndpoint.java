package ru.tsc.borisyuk.tm.api.endpoint;

import lombok.NonNull;
import ru.tsc.borisyuk.tm.dto.request.*;
import ru.tsc.borisyuk.tm.dto.response.*;


public interface IDomainEndpoint {

    @NonNull
    DataBackupLoadResponse loadDataBackup(@NonNull DataBackupLoadRequest request);

    @NonNull
    DataBase64LoadResponse loadDataBase64(@NonNull DataBase64LoadRequest request);

    @NonNull
    DataBinaryLoadResponse loadDataBinary(@NonNull DataBinaryLoadRequest request);

    @NonNull
    DataJsonFasterLoadResponse loadDataJsonFaster(@NonNull DataJsonFasterLoadRequest request);

    @NonNull
    DataJsonJaxbLoadResponse loadDataJsonJaxb(@NonNull DataJsonJaxbLoadRequest request);

    @NonNull
    DataXmlFasterLoadResponse loadDataXmlFaster(@NonNull DataXmlFasterLoadRequest request);

    @NonNull
    DataXmlJaxbLoadResponse loadDataXmlJaxb(@NonNull DataXmlJaxbLoadRequest request);

    @NonNull
    DataYamlFasterLoadResponse loadDataYamlFaster(@NonNull DataYamlFasterLoadRequest request);

    @NonNull
    DataBackupSaveResponse saveDataBackup(@NonNull DataBackupSaveRequest request);

    @NonNull
    DataBase64SaveResponse saveDataBase64(@NonNull DataBase64SaveRequest request);

    @NonNull
    DataBinarySaveResponse saveDataBinary(@NonNull DataBinarySaveRequest request);

    @NonNull
    DataJsonFasterSaveResponse saveDataJsonFaster(@NonNull DataJsonFasterSaveRequest request);

    @NonNull
    DataJsonJaxbSaveResponse saveDataJsonJaxb(@NonNull DataJsonJaxbSaveRequest request);

    @NonNull
    DataXmlFasterSaveResponse saveDataXmlFaster(@NonNull DataXmlFasterSaveRequest request);

    @NonNull
    DataXmlJaxbSaveResponse saveDataXmlJaxb(@NonNull DataXmlJaxbSaveRequest request);

    @NonNull
    DataYamlFasterSaveResponse saveDataYamlFaster(@NonNull DataYamlFasterSaveRequest request);

}

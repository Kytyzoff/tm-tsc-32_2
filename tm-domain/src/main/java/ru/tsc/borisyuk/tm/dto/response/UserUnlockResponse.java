package ru.tsc.borisyuk.tm.dto.response;

import lombok.NoArgsConstructor;
import ru.tsc.borisyuk.tm.model.User;

@NoArgsConstructor
public class UserUnlockResponse extends AbstractUserResponse {

    public UserUnlockResponse(final User user) {
        super();
    }

}
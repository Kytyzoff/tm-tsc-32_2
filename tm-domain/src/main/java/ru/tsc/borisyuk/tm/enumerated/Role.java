package ru.tsc.borisyuk.tm.enumerated;

import lombok.Getter;
import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;

@Getter
public enum Role {

    USUAL("Usual user"),
    ADMIN("Administrator");

    private final String displayName;

    Role(final String displayName) {
        this.displayName = displayName;
    }

    public static Role toRole(final String value) {
        if (StringUtils.isEmpty(value)) return null;
        return Arrays.stream(values())
                .filter(role -> role.name().equals(value))
                .findAny().orElse(null);
    }

    public static String toName(final Role role) {
        if (role == null) return "";
        return role.getDisplayName();
    }

}

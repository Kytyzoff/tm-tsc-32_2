package ru.tsc.borisyuk.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class TaskUnbindFromProjectRequest extends AbstractUserRequest {

    private String taskId;

    private String projectId;

}
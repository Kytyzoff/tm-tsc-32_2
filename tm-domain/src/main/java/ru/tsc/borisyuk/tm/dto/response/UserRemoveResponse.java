package ru.tsc.borisyuk.tm.dto.response;

import lombok.NoArgsConstructor;
import ru.tsc.borisyuk.tm.model.User;

@NoArgsConstructor
public class UserRemoveResponse extends AbstractUserResponse {

    public UserRemoveResponse(final User user) {
        super(user);
    }

}
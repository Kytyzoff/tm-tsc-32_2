package ru.tsc.borisyuk.tm.dto.response;

import lombok.NoArgsConstructor;
import ru.tsc.borisyuk.tm.model.Task;

@NoArgsConstructor
public class TaskUnbindFromProjectResponse extends AbstractTaskResponse {

    public TaskUnbindFromProjectResponse(final Task task) {
        super(task);
    }

}
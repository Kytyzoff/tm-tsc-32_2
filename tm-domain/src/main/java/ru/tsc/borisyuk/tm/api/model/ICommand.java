package ru.tsc.borisyuk.tm.api.model;

import lombok.NonNull;

public interface ICommand {

    @NonNull
    String getName();

    @NonNull
    String getDescription();

    String getArgument();

}

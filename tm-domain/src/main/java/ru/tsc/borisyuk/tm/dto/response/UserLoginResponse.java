package ru.tsc.borisyuk.tm.dto.response;

import lombok.NoArgsConstructor;
import lombok.NonNull;

@NoArgsConstructor
public class UserLoginResponse extends AbstractResultResponse {

    public UserLoginResponse(@NonNull final Throwable throwable) {
        super(throwable);
    }

}

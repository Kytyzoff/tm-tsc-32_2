package ru.tsc.borisyuk.tm.model;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

import java.io.Serializable;
import java.util.UUID;

@Getter
@Setter
public class AbstractModel implements Serializable {

    @NonNull
    private static final long serialVersionUID = 1;

    @NonNull
    private String id = UUID.randomUUID().toString();

}

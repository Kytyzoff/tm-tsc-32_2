package ru.tsc.borisyuk.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.tsc.borisyuk.tm.enumerated.Status;

@Getter
@Setter
@NoArgsConstructor
public class ProjectChangeStatusByIndexRequest extends AbstractUserRequest {

    private Integer index;

    private Status status;

}

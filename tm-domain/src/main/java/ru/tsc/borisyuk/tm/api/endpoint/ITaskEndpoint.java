package ru.tsc.borisyuk.tm.api.endpoint;

import lombok.NonNull;
import ru.tsc.borisyuk.tm.dto.request.*;
import ru.tsc.borisyuk.tm.dto.response.*;

public interface ITaskEndpoint {

    @NonNull
    TaskChangeStatusByIdResponse taskChangeStatusById(@NonNull final TaskChangeStatusByIdRequest request);

    @NonNull
    TaskChangeStatusByIndexResponse taskChangeStatusByIndex(@NonNull final TaskChangeStatusByIndexRequest request);

    @NonNull
    TaskRemoveByIdResponse taskRemoveById(@NonNull final TaskRemoveByIdRequest request);

    @NonNull
    TaskRemoveByIndexResponse taskRemoveByIndex(@NonNull final TaskRemoveByIndexRequest request);

    @NonNull
    TaskGetByIdResponse taskGetById(@NonNull final TaskGetByIdRequest request);

    @NonNull
    TaskGetByIndexResponse taskGetByIndex(@NonNull final TaskGetByIndexRequest request);

    @NonNull
    TaskUpdateByIdResponse taskUpdateById(@NonNull final TaskUpdateByIdRequest request);

    @NonNull
    TaskUpdateByIndexResponse taskUpdateByIndex(@NonNull final TaskUpdateByIndexRequest request);

    @NonNull
    TaskCreateResponse taskCreate(@NonNull final TaskCreateRequest request);

    @NonNull
    TaskListResponse taskList(@NonNull final TaskListRequest request);

    @NonNull
    TaskBindToProjectResponse taskBindToProject(final @NonNull TaskBindToProjectRequest request);

    @NonNull
    TaskUnbindFromProjectResponse taskUnbindFromProject(final @NonNull TaskUnbindFromProjectRequest request);

    @NonNull
    TaskClearResponse taskClear(@NonNull final TaskClearRequest request);

}

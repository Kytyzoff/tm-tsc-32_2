package ru.tsc.borisyuk.tm.model;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class AbstractUserOwnedModel extends AbstractModel implements Serializable {

    @NonNull
    private static final long serialVersionUID = 1;

    private String userId;

}

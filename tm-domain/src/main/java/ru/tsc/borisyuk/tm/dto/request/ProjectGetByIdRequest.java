package ru.tsc.borisyuk.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ProjectGetByIdRequest extends AbstractUserRequest {

    private String id;

}

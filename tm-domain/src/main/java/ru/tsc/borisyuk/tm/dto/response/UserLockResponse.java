package ru.tsc.borisyuk.tm.dto.response;

import lombok.NoArgsConstructor;
import ru.tsc.borisyuk.tm.model.User;

@NoArgsConstructor
public class UserLockResponse extends AbstractUserResponse {

    public UserLockResponse(final User user) {
        super();
    }

}
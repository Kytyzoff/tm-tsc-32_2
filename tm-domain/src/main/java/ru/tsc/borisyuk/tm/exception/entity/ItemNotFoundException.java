package ru.tsc.borisyuk.tm.exception.entity;

public final class ItemNotFoundException extends AbstractEntityNotFoundException {

    public ItemNotFoundException() {
        super("Error! Entity not found...");
    }

}

package ru.tsc.borisyuk.tm.api.endpoint;

import lombok.NonNull;
import ru.tsc.borisyuk.tm.dto.request.*;
import ru.tsc.borisyuk.tm.dto.response.*;

public interface IProjectEndpoint {

    @NonNull
    ProjectChangeStatusByIdResponse projectChangeStatusById(@NonNull final ProjectChangeStatusByIdRequest request);

    @NonNull
    ProjectChangeStatusByIndexResponse projectChangeStatusByIndex(@NonNull final ProjectChangeStatusByIndexRequest request);

    @NonNull
    ProjectRemoveByIdResponse projectRemoveById(@NonNull final ProjectRemoveByIdRequest request);

    @NonNull
    ProjectRemoveByIndexResponse projectRemoveByIndex(@NonNull final ProjectRemoveByIndexRequest request);

    @NonNull
    ProjectGetByIdResponse projectGetById(@NonNull final ProjectGetByIdRequest request);

    @NonNull
    ProjectGetByIndexResponse projectGetByIndex(@NonNull final ProjectGetByIndexRequest request);

    @NonNull
    ProjectUpdateByIdResponse projectUpdateById(@NonNull final ProjectUpdateByIdRequest request);

    @NonNull
    ProjectUpdateByIndexResponse projectUpdateByIndex(@NonNull final ProjectUpdateByIndexRequest request);

    @NonNull
    ProjectCreateResponse projectCreate(@NonNull final ProjectCreateRequest request);

    @NonNull
    ProjectListResponse projectList(@NonNull final ProjectListRequest request);

    @NonNull
    ProjectClearResponse projectClear(@NonNull final ProjectClearRequest request);

}

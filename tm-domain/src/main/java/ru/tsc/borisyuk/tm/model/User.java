package ru.tsc.borisyuk.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import ru.tsc.borisyuk.tm.enumerated.Role;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
public final class User extends AbstractModel implements Serializable {

    @NonNull
    private static final long serialVersionUID = 1;

    private String login;

    private String passwordHash;

    private String email;

    private String firstName;

    private String middleName;

    private String lastName;

    @NonNull
    private Role role = Role.USUAL;

    private boolean locked = false;

}

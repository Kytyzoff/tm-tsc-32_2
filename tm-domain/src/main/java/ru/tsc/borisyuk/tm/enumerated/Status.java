package ru.tsc.borisyuk.tm.enumerated;

import lombok.Getter;
import lombok.NonNull;
import org.apache.commons.lang3.StringUtils;
import ru.tsc.borisyuk.tm.exception.field.StatusEmptyException;

import java.util.Arrays;

@Getter
public enum Status {

    NOT_STARTED("Not started"),
    IN_PROGRESS("In progress"),
    COMPLETED("Completed");

    private String displayName;

    Status(final String displayName) {
        this.displayName = displayName;
    }

    @NonNull
    public static Status toStatus(final String value) {
        if (StringUtils.isEmpty(value)) return null;
        return Arrays.stream(values())
                .filter(status -> status.name().equals(value))
                .findAny().orElseThrow(StatusEmptyException::new);
    }

    public static String toName(final Status status) {
        if (status == null) return "";
        return status.getDisplayName();
    }

}

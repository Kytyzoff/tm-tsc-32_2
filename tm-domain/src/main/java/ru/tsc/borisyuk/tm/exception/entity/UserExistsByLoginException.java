package ru.tsc.borisyuk.tm.exception.entity;

public final class UserExistsByLoginException extends AbstractEntityExistsException {

    public UserExistsByLoginException() {
        super("Error! User login already exists...");
    }

}

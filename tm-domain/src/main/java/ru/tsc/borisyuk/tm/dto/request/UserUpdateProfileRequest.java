package ru.tsc.borisyuk.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class UserUpdateProfileRequest extends AbstractUserRequest {

    private String firstName;

    private String lastName;

    private String middleName;

}
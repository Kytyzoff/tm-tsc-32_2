package ru.tsc.borisyuk.tm.dto.response;

import lombok.NoArgsConstructor;
import ru.tsc.borisyuk.tm.model.Task;

@NoArgsConstructor
public class TaskRemoveByIdResponse extends AbstractTaskResponse {

    public TaskRemoveByIdResponse(final Task task) {
        super(task);
    }

}

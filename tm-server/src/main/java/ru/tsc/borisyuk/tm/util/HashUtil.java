package ru.tsc.borisyuk.tm.util;

import lombok.NonNull;
import org.apache.commons.lang3.ObjectUtils;
import ru.tsc.borisyuk.tm.api.service.IPropertyService;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public interface HashUtil {

    static String salt(
            final String value,
            final String secret,
            final Integer iteration
    ) {
        if (ObjectUtils.anyNull(value, secret, iteration)) return null;
        String result = value;
        for (int i = 0; i < iteration; i++) {
            result = mds5(secret + result + secret);
        }
        return result;
    }

    static String mds5(String value) {
        if (value == null) return null;
        try {
            @NonNull final MessageDigest md = MessageDigest.getInstance("MD5");
            @NonNull final byte[] array = md.digest(value.getBytes());
            @NonNull final StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; i++) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100)).substring(1, 3);
            }
            return sb.toString();
        } catch (@NonNull final NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    static String salt(IPropertyService propertyService, String value) {
        if (propertyService == null) return null;
        return salt(value, propertyService.getPasswordSecret(), propertyService.getPasswordIteration());
    }

}

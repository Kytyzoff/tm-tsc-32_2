package ru.tsc.borisyuk.tm.task;

import lombok.NonNull;
import ru.tsc.borisyuk.tm.component.Server;

public abstract class AbstractServerTask implements Runnable {

    @NonNull
    protected final Server server;

    public AbstractServerTask(@NonNull final Server server) {
        this.server = server;
    }

}

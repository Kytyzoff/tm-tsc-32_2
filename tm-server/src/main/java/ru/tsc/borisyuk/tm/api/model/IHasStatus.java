package ru.tsc.borisyuk.tm.api.model;

import lombok.NonNull;
import ru.tsc.borisyuk.tm.enumerated.Status;

public interface IHasStatus {

    @NonNull
    Status getStatus();

    void setStatus(@NonNull Status status);

}

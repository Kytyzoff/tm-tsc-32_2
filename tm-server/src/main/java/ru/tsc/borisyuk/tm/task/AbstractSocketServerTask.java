package ru.tsc.borisyuk.tm.task;

import lombok.NonNull;
import ru.tsc.borisyuk.tm.component.Server;

import java.net.Socket;

public abstract class AbstractSocketServerTask extends AbstractServerTask {

    @NonNull
    protected Socket socket;

    protected String userId;

    public AbstractSocketServerTask(@NonNull final Server server, @NonNull final Socket socket) {
        super(server);
        this.socket = socket;
    }

    public AbstractSocketServerTask(
            @NonNull final Server server,
            @NonNull final Socket socket,
            final String userId
    ) {
        super(server);
        this.socket = socket;
        this.userId = userId;
    }

}

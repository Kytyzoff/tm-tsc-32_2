package ru.tsc.borisyuk.tm.api.model;

import lombok.NonNull;

import java.util.Date;

public interface IHasCreated {

    @NonNull
    Date getCreated();

    void setCreated(@NonNull Date created);

}

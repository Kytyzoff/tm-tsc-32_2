package ru.tsc.borisyuk.tm.api.repository;

import ru.tsc.borisyuk.tm.model.User;

public interface IUserRepository extends IRepository<User> {

    User findById(String id);

    User findByLogin(String login);

    User findByEmail(String email);

    boolean existsByLogin(String login);

    boolean existsByEmail(String email);

}

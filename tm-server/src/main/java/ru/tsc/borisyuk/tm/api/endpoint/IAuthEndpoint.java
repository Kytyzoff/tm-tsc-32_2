package ru.tsc.borisyuk.tm.api.endpoint;

import lombok.NonNull;
import ru.tsc.borisyuk.tm.dto.request.UserLoginRequest;
import ru.tsc.borisyuk.tm.dto.request.UserLogoutRequest;
import ru.tsc.borisyuk.tm.dto.request.UserProfileRequest;
import ru.tsc.borisyuk.tm.dto.response.UserLoginResponse;
import ru.tsc.borisyuk.tm.dto.response.UserLogoutResponse;
import ru.tsc.borisyuk.tm.dto.response.UserProfileResponse;

public interface IAuthEndpoint {

    @NonNull
    UserLoginResponse login(@NonNull UserLoginRequest request);

    @NonNull
    UserLogoutResponse logout(@NonNull UserLogoutRequest request);

    @NonNull
    UserProfileResponse profile(@NonNull UserProfileRequest request);

}

package ru.tsc.borisyuk.tm.endpoint;

import lombok.NonNull;
import ru.tsc.borisyuk.tm.api.endpoint.IDomainEndpoint;
import ru.tsc.borisyuk.tm.api.service.IServiceLocator;
import ru.tsc.borisyuk.tm.dto.request.*;
import ru.tsc.borisyuk.tm.dto.response.*;
import ru.tsc.borisyuk.tm.enumerated.Role;

public final class DomainEndpoint extends AbstractEndpoint implements IDomainEndpoint {

    public DomainEndpoint(final @NonNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NonNull
    @Override
    public DataBackupLoadResponse loadDataBackup(@NonNull final DataBackupLoadRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataBackup();
        return new DataBackupLoadResponse();
    }

    @NonNull
    @Override
    public DataBase64LoadResponse loadDataBase64(@NonNull final DataBase64LoadRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataBase64();
        return new DataBase64LoadResponse();
    }

    @NonNull
    @Override
    public DataBinaryLoadResponse loadDataBinary(@NonNull final DataBinaryLoadRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataBinary();
        return new DataBinaryLoadResponse();
    }

    @NonNull
    @Override
    public DataJsonFasterLoadResponse loadDataJsonFaster(@NonNull final DataJsonFasterLoadRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataJsonFaster();
        return new DataJsonFasterLoadResponse();
    }

    @NonNull
    @Override
    public DataJsonJaxbLoadResponse loadDataJsonJaxb(@NonNull final DataJsonJaxbLoadRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataJsonJaxb();
        return new DataJsonJaxbLoadResponse();
    }

    @NonNull
    @Override
    public DataXmlFasterLoadResponse loadDataXmlFaster(@NonNull final DataXmlFasterLoadRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataXmlFaster();
        return new DataXmlFasterLoadResponse();
    }

    @NonNull
    @Override
    public DataXmlJaxbLoadResponse loadDataXmlJaxb(@NonNull final DataXmlJaxbLoadRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataXmlJaxb();
        return new DataXmlJaxbLoadResponse();
    }

    @NonNull
    @Override
    public DataYamlFasterLoadResponse loadDataYamlFaster(@NonNull final DataYamlFasterLoadRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataYamlFaster();
        return new DataYamlFasterLoadResponse();
    }

    @NonNull
    @Override
    public DataBackupSaveResponse saveDataBackup(@NonNull final DataBackupSaveRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataBackup();
        return new DataBackupSaveResponse();
    }

    @NonNull
    @Override
    public DataBase64SaveResponse saveDataBase64(@NonNull final DataBase64SaveRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataBase64();
        return new DataBase64SaveResponse();
    }

    @NonNull
    @Override
    public DataBinarySaveResponse saveDataBinary(@NonNull final DataBinarySaveRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataBinary();
        return new DataBinarySaveResponse();
    }

    @NonNull
    @Override
    public DataJsonFasterSaveResponse saveDataJsonFaster(@NonNull final DataJsonFasterSaveRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataJsonFaster();
        return new DataJsonFasterSaveResponse();
    }

    @NonNull
    @Override
    public DataJsonJaxbSaveResponse saveDataJsonJaxb(@NonNull final DataJsonJaxbSaveRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataJsonJaxb();
        return new DataJsonJaxbSaveResponse();
    }

    @NonNull
    @Override
    public DataXmlFasterSaveResponse saveDataXmlFaster(@NonNull final DataXmlFasterSaveRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataXmlFaster();
        return new DataXmlFasterSaveResponse();
    }

    @NonNull
    @Override
    public DataXmlJaxbSaveResponse saveDataXmlJaxb(@NonNull final DataXmlJaxbSaveRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataXmlJaxb();
        return new DataXmlJaxbSaveResponse();
    }

    @NonNull
    @Override
    public DataYamlFasterSaveResponse saveDataYamlFaster(@NonNull final DataYamlFasterSaveRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataYamlFaster();
        return new DataYamlFasterSaveResponse();
    }

}

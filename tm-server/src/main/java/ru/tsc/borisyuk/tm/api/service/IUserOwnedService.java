package ru.tsc.borisyuk.tm.api.service;

import ru.tsc.borisyuk.tm.api.repository.IUserOwnedRepository;
import ru.tsc.borisyuk.tm.enumerated.Sort;
import ru.tsc.borisyuk.tm.model.AbstractUserOwnedModel;

import java.util.List;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IUserOwnedRepository<M>, IService<M> {

    M removeById(String userId, String id);

    M removeByIndex(String userId, Integer index);

    List<M> findAll(String userId, Sort sort);

}

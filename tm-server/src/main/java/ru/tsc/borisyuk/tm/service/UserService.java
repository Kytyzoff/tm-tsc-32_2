package ru.tsc.borisyuk.tm.service;

import lombok.NonNull;
import org.apache.commons.lang3.StringUtils;
import ru.tsc.borisyuk.tm.api.repository.IProjectRepository;
import ru.tsc.borisyuk.tm.api.repository.ITaskRepository;
import ru.tsc.borisyuk.tm.api.repository.IUserRepository;
import ru.tsc.borisyuk.tm.api.service.IPropertyService;
import ru.tsc.borisyuk.tm.api.service.IUserService;
import ru.tsc.borisyuk.tm.enumerated.Role;
import ru.tsc.borisyuk.tm.exception.entity.UserExistsByEmailException;
import ru.tsc.borisyuk.tm.exception.entity.UserExistsByLoginException;
import ru.tsc.borisyuk.tm.exception.entity.UserNotFoundException;
import ru.tsc.borisyuk.tm.exception.field.*;
import ru.tsc.borisyuk.tm.model.User;
import ru.tsc.borisyuk.tm.util.HashUtil;

public class UserService extends AbstractService<User, IUserRepository> implements IUserService {

    @NonNull
    private final ITaskRepository taskRepository;

    @NonNull
    private final IProjectRepository projectRepository;

    @NonNull
    private final IPropertyService propertyService;

    public UserService(
            @NonNull final IUserRepository userRepository,
            @NonNull final ITaskRepository taskRepository,
            @NonNull final IProjectRepository projectRepository,
            @NonNull final IPropertyService propertyService
    ) {
        super(userRepository);
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
        this.propertyService = propertyService;
    }

    @Override
    public User findOneByLogin(final String login) {
        if (StringUtils.isEmpty(login)) throw new LoginEmptyException();
        return repository.findByLogin(login);
    }

    @Override
    public User findOneByEmail(final String email) {
        if (StringUtils.isEmpty(email)) throw new EmailEmptyException();
        return repository.findByEmail(email);
    }

    @Override
    public User removeByLogin(final String login) {
        if (StringUtils.isEmpty(login)) throw new EmailEmptyException();
        final User user = findOneByLogin(login);
        if (user == null) throw new UserNotFoundException();
        return remove(user);
    }

    @Override
    public User remove(final User user) {
        if (user == null) throw new UserNotFoundException();
        final User userToRemove = super.remove(user);
        if (userToRemove == null) return null;
        @NonNull final String userId = userToRemove.getId();
        taskRepository.clear(userId);
        projectRepository.clear(userId);
        return userToRemove;
    }

    @Override
    public User create(final String login, final String password) {
        if (StringUtils.isEmpty(login)) throw new LoginEmptyException();
        if (StringUtils.isEmpty(password)) throw new PasswordEmptyException();
        if (repository.existsByLogin(login)) throw new UserExistsByLoginException();
        @NonNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        return repository.add(user);
    }

    @Override
    public User create(final String login, final String password, final String email) {
        if (StringUtils.isEmpty(login)) throw new LoginEmptyException();
        if (StringUtils.isEmpty(password)) throw new PasswordEmptyException();
        if (StringUtils.isEmpty(email)) throw new EmailEmptyException();
        if (repository.existsByLogin(login)) throw new UserExistsByLoginException();
        if (repository.existsByEmail(email)) throw new UserExistsByEmailException();
        @NonNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setEmail(email);
        return repository.add(user);
    }

    @Override
    public User create(final String login, final String password, final Role role) {
        if (StringUtils.isEmpty(login)) throw new LoginEmptyException();
        if (StringUtils.isEmpty(password)) throw new PasswordEmptyException();
        if (role == null) throw new RoleEmptyException();
        if (repository.existsByLogin(login)) throw new UserExistsByLoginException();
        @NonNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(role);
        return repository.add(user);
    }

    @Override
    public User create(final String login, final String password, final String email, final Role role) {
        if (StringUtils.isEmpty(login)) throw new LoginEmptyException();
        if (StringUtils.isEmpty(password)) throw new PasswordEmptyException();
        if (StringUtils.isEmpty(email)) throw new EmailEmptyException();
        if (role == null) throw new RoleEmptyException();
        if (repository.existsByLogin(login)) throw new UserExistsByLoginException();
        if (repository.existsByEmail(email)) throw new UserExistsByEmailException();
        @NonNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setEmail(email);
        user.setRole(role);
        return repository.add(user);
    }

    @Override
    public User setPassword(final String userId, final String password) {
        if (StringUtils.isEmpty(userId)) throw new UserIdEmptyException();
        if (StringUtils.isEmpty(password)) throw new PasswordEmptyException();
        final User user = findOneById(userId);
        if (user == null) throw new UserNotFoundException();
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        return user;
    }

    @Override
    public User updateUser(
            final String userId, final String firstName,
            final String lastName, final String middleName
    ) {
        if (StringUtils.isEmpty(userId)) throw new UserIdEmptyException();
        final User user = findOneById(userId);
        if (user == null) throw new UserNotFoundException();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        return user;
    }

    @Override
    public User lockUserByLogin(final String login) {
        if (StringUtils.isEmpty(login)) throw new LoginEmptyException();
        final User user = findOneByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(true);
        return user;
    }

    @Override
    public User unlockUserByLogin(final String login) {
        if (StringUtils.isEmpty(login)) throw new LoginEmptyException();
        final User user = findOneByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(false);
        return user;
    }

}

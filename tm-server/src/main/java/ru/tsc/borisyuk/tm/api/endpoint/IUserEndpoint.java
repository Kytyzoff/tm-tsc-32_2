package ru.tsc.borisyuk.tm.api.endpoint;

import lombok.NonNull;
import ru.tsc.borisyuk.tm.dto.request.*;
import ru.tsc.borisyuk.tm.dto.response.*;

public interface IUserEndpoint {

    @NonNull
    UserLockResponse lockUser(@NonNull UserLockRequest request);

    @NonNull
    UserUnlockResponse unlockUser(@NonNull UserUnlockRequest request);

    @NonNull
    UserChangePasswordResponse changeUserPassword(@NonNull UserChangePasswordRequest request);

    @NonNull
    UserRegisterResponse registerUser(@NonNull UserRegisterRequest request);

    @NonNull
    UserRemoveResponse removeUser(@NonNull UserRemoveRequest request);

    @NonNull
    UserUpdateProfileResponse updateUserProfile(@NonNull UserUpdateProfileRequest request);

}
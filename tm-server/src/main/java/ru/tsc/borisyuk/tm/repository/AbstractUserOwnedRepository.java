package ru.tsc.borisyuk.tm.repository;

import lombok.NonNull;
import ru.tsc.borisyuk.tm.api.repository.IUserOwnedRepository;
import ru.tsc.borisyuk.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M> implements IUserOwnedRepository<M> {

    @NonNull
    @Override
    public List<M> findAll(@NonNull final String userId) {
        @NonNull final List<M> result = items.stream()
                .filter(item -> userId.equals(item.getUserId()))
                .collect(Collectors.toList());
        return result;
    }

    @NonNull
    @Override
    public List<M> findAll(@NonNull final String userId, @NonNull final Comparator comparator) {
        @NonNull final List<M> result = findAll(userId);
        result.sort(comparator);
        return result;
    }

    @NonNull
    @Override
    public M add(@NonNull final String userId, @NonNull final M item) {
        item.setUserId(userId);
        return add(item);
    }

    @Override
    public M remove(@NonNull final String userId, @NonNull final M item) {
        if (userId != item.getUserId()) return null;
        return remove(item);
    }

    @Override
    public void clear(@NonNull final String userId) {
        @NonNull final List<M> items = findAll(userId);
        removeAll(items);
    }

    @NonNull
    @Override
    public M findOneByIndex(@NonNull final String userId, @NonNull final Integer index) {
        return findAll(userId).get(index);
    }

    @Override
    public M findOneById(@NonNull final String userId, @NonNull final String id) {
        final M result = items.stream()
                .filter(item -> userId.equals(item.getUserId()) && id.equals(item.getId()))
                .findFirst().orElse(null);
        return result;
    }

    @Override
    public int size(final String userId) {
        return findAll(userId).size();
    }

}

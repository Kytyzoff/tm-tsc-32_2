package ru.tsc.borisyuk.tm.component;

import lombok.Getter;
import lombok.NonNull;
import lombok.SneakyThrows;
import ru.tsc.borisyuk.tm.api.service.*;
import ru.tsc.borisyuk.tm.repository.ProjectRepository;
import ru.tsc.borisyuk.tm.repository.TaskRepository;
import ru.tsc.borisyuk.tm.repository.UserRepository;
import ru.tsc.borisyuk.tm.api.endpoint.IDomainEndpoint;
import ru.tsc.borisyuk.tm.api.endpoint.IProjectEndpoint;
import ru.tsc.borisyuk.tm.api.endpoint.ISystemEndpoint;
import ru.tsc.borisyuk.tm.api.endpoint.ITaskEndpoint;
import ru.tsc.borisyuk.tm.api.repository.IProjectRepository;
import ru.tsc.borisyuk.tm.api.repository.ITaskRepository;
import ru.tsc.borisyuk.tm.api.repository.IUserRepository;
import ru.tsc.borisyuk.tm.service.*;
import ru.tsc.borisyuk.tm.dto.request.*;
import ru.tsc.borisyuk.tm.endpoint.DomainEndpoint;
import ru.tsc.borisyuk.tm.endpoint.ProjectEndpoint;
import ru.tsc.borisyuk.tm.endpoint.SystemEndpoint;
import ru.tsc.borisyuk.tm.endpoint.TaskEndpoint;
import ru.tsc.borisyuk.tm.enumerated.Role;
import ru.tsc.borisyuk.tm.model.User;
import ru.tsc.borisyuk.tm.util.SystemUtil;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Bootstrap implements IServiceLocator {

    @NonNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @Getter
    @NonNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NonNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @Getter
    @NonNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @Getter
    @NonNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @Getter
    @NonNull
    private final ILoggerService loggerService = new LoggerService();

    @NonNull
    private final IUserRepository userRepository = new UserRepository();

    @Getter
    @NonNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NonNull
    private final IUserService userService = new UserService(userRepository, taskRepository, projectRepository, propertyService);

    @Getter
    @NonNull
    private final IAuthService authService = new AuthService(propertyService, userService);

    @Getter
    @NonNull
    private final IDomainService domainService = new DomainService();

    @NonNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NonNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpoint(this);

    @NonNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NonNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

//    @NonNull
//    private final IAuthEndpoint authEndpoint = new AuthEndpointClient();

    @NonNull
    private final Backup backup = new Backup(this);

    @NonNull
    private final Server server = new Server(this);

    {
        server.register(ServerAboutRequest.class, systemEndpoint::getAbout);
        server.register(ServerVersionRequest.class, systemEndpoint::getVersion);
        server.register(DataBackupLoadRequest.class, domainEndpoint::loadDataBackup);
        server.register(DataBase64LoadRequest.class, domainEndpoint::loadDataBase64);
        server.register(DataBinaryLoadRequest.class, domainEndpoint::loadDataBinary);
        server.register(DataJsonFasterLoadRequest.class, domainEndpoint::loadDataJsonFaster);
        server.register(DataJsonJaxbLoadRequest.class, domainEndpoint::loadDataJsonJaxb);
        server.register(DataXmlFasterLoadRequest.class, domainEndpoint::loadDataXmlFaster);
        server.register(DataXmlJaxbLoadRequest.class, domainEndpoint::loadDataXmlJaxb);
        server.register(DataYamlFasterLoadRequest.class, domainEndpoint::loadDataYamlFaster);
        server.register(DataBackupSaveRequest.class, domainEndpoint::saveDataBackup);
        server.register(DataBase64SaveRequest.class, domainEndpoint::saveDataBase64);
        server.register(DataBinarySaveRequest.class, domainEndpoint::saveDataBinary);
        server.register(DataJsonFasterSaveRequest.class, domainEndpoint::saveDataJsonFaster);
        server.register(DataJsonJaxbSaveRequest.class, domainEndpoint::saveDataJsonJaxb);
        server.register(DataXmlFasterSaveRequest.class, domainEndpoint::saveDataXmlFaster);
        server.register(DataXmlJaxbSaveRequest.class, domainEndpoint::saveDataXmlJaxb);
        server.register(DataYamlFasterSaveRequest.class, domainEndpoint::saveDataYamlFaster);
//        server.register(UserLoginRequest.class, authEndpoint::login);
//        server.register(UserLogoutRequest.class, authEndpoint::logout);
//        server.register(UserProfileRequest.class, authEndpoint::profile);
        server.register(ProjectChangeStatusByIdRequest.class, projectEndpoint::projectChangeStatusById);
        server.register(ProjectChangeStatusByIndexRequest.class, projectEndpoint::projectChangeStatusByIndex);
        server.register(ProjectRemoveByIdRequest.class, projectEndpoint::projectRemoveById);
        server.register(ProjectRemoveByIndexRequest.class, projectEndpoint::projectRemoveByIndex);
        server.register(ProjectGetByIdRequest.class, projectEndpoint::projectGetById);
        server.register(ProjectGetByIndexRequest.class, projectEndpoint::projectGetByIndex);
        server.register(ProjectUpdateByIdRequest.class, projectEndpoint::projectUpdateById);
        server.register(ProjectUpdateByIndexRequest.class, projectEndpoint::projectUpdateByIndex);
        server.register(ProjectCreateRequest.class, projectEndpoint::projectCreate);
        server.register(ProjectListRequest.class, projectEndpoint::projectList);
        server.register(ProjectClearRequest.class, projectEndpoint::projectClear);
        server.register(TaskChangeStatusByIdRequest.class, taskEndpoint::taskChangeStatusById);
        server.register(TaskChangeStatusByIndexRequest.class, taskEndpoint::taskChangeStatusByIndex);
        server.register(TaskRemoveByIdRequest.class, taskEndpoint::taskRemoveById);
        server.register(TaskRemoveByIndexRequest.class, taskEndpoint::taskRemoveByIndex);
        server.register(TaskGetByIdRequest.class, taskEndpoint::taskGetById);
        server.register(TaskGetByIndexRequest.class, taskEndpoint::taskGetByIndex);
        server.register(TaskUpdateByIdRequest.class, taskEndpoint::taskUpdateById);
        server.register(TaskUpdateByIndexRequest.class, taskEndpoint::taskUpdateByIndex);
        server.register(TaskCreateRequest.class, taskEndpoint::taskCreate);
        server.register(TaskListRequest.class, taskEndpoint::taskList);
        server.register(TaskClearRequest.class, taskEndpoint::taskClear);
        server.register(TaskBindToProjectRequest.class, taskEndpoint::taskBindToProject);
        server.register(TaskUnbindFromProjectRequest.class, taskEndpoint::taskUnbindFromProject);
    }

    public void run() {
        loggerService.info("Task Manager Server started");
        initPID();
        initUsers();
        backup.start();
        server.start();
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
    }

    private void prepareShutdown() {
        backup.stop();
        server.stop();
        loggerService.info("Task Manager Server is shutting down...");
    }

    @SneakyThrows
    private void initPID() {
        @NonNull final String filename = "task-manager.pid";
        @NonNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NonNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void initUsers() {
        userService.create("user", "user");
        userService.create("test", "test", "test@example.com");
        @NonNull final User admin = userService.create("admin", "admin", "admin@example.com", Role.ADMIN);
        admin.setFirstName("Иван");
        admin.setLastName("Петров");
    }

}

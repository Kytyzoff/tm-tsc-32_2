package ru.tsc.borisyuk.tm.api.component;

import lombok.NonNull;

public interface ISaltProvider {

    @NonNull
    public String getPasswordSecret();

    @NonNull
    public Integer getPasswordIteration();

}

package ru.tsc.borisyuk.tm.api.service;

import ru.tsc.borisyuk.tm.model.User;

public interface IAuthService {

    User register(String login, String password, String email);

    User check(String login, String password);

}

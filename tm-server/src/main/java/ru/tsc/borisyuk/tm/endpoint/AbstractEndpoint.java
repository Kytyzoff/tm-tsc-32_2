package ru.tsc.borisyuk.tm.endpoint;

import lombok.Getter;
import lombok.NonNull;
import org.apache.commons.lang3.StringUtils;
import ru.tsc.borisyuk.tm.api.service.IProjectService;
import ru.tsc.borisyuk.tm.api.service.IServiceLocator;
import ru.tsc.borisyuk.tm.api.service.ITaskService;
import ru.tsc.borisyuk.tm.api.service.IUserService;
import ru.tsc.borisyuk.tm.dto.request.AbstractUserRequest;
import ru.tsc.borisyuk.tm.enumerated.Role;
import ru.tsc.borisyuk.tm.exception.system.AccessDeniedException;
import ru.tsc.borisyuk.tm.model.User;

public abstract class AbstractEndpoint {

    @Getter
    @NonNull
    private final IServiceLocator serviceLocator;

    public AbstractEndpoint(@NonNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    protected void check(AbstractUserRequest request, Role role) {
        if (request == null) throw new AccessDeniedException();
        if (role == null) throw new AccessDeniedException();
        final String userId = request.getUserId();
        if (StringUtils.isEmpty(userId)) throw new AccessDeniedException();
        @NonNull final IUserService userService = getServiceLocator().getUserService();
        final User user = userService.findOneById(userId);
        if (user == null) throw new AccessDeniedException();
        final Role userRole = user.getRole();
        boolean check = userRole == role;
        if (!check) throw new AccessDeniedException();
    }

    protected void check(AbstractUserRequest request) {
        if (request == null) throw new AccessDeniedException();
        final String userId = request.getUserId();
        if (StringUtils.isEmpty(userId)) throw new AccessDeniedException();
    }

    @NonNull
    protected IProjectService getProjectService() {
        return serviceLocator.getProjectService();
    }

    @NonNull
    protected ITaskService getTaskService() {
        return serviceLocator.getTaskService();
    }

}

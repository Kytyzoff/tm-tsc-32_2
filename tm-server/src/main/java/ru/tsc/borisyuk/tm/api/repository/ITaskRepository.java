package ru.tsc.borisyuk.tm.api.repository;

import lombok.NonNull;
import ru.tsc.borisyuk.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IUserOwnedRepository<Task> {

    @NonNull
    Task create(String userId, String name);

    @NonNull
    Task create(String userId, String name, String description);

    @NonNull
    List<Task> findAllByProjectId(String userId, String projectId);

    boolean existsById(String userId, String id);

}

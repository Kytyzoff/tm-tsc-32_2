package ru.tsc.borisyuk.tm.service;

import lombok.NonNull;
import org.apache.commons.lang3.StringUtils;
import ru.tsc.borisyuk.tm.api.repository.IProjectRepository;
import ru.tsc.borisyuk.tm.api.repository.ITaskRepository;
import ru.tsc.borisyuk.tm.api.service.IProjectTaskService;
import ru.tsc.borisyuk.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.borisyuk.tm.exception.entity.TaskNotFoundException;
import ru.tsc.borisyuk.tm.exception.field.ProjectIdEmptyException;
import ru.tsc.borisyuk.tm.exception.field.TaskIdEmptyException;
import ru.tsc.borisyuk.tm.exception.field.UserIdEmptyException;
import ru.tsc.borisyuk.tm.model.Project;
import ru.tsc.borisyuk.tm.model.Task;

import java.util.List;

public class ProjectTaskService implements IProjectTaskService {

    @NonNull
    private IProjectRepository projectRepository;

    @NonNull
    private ITaskRepository taskRepository;

    public ProjectTaskService(
            @NonNull final IProjectRepository projectRepository,
            @NonNull final ITaskRepository taskRepository
    ) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public Task bindTaskToProject(final String userId, final String projectId, final String taskId) {
        if (StringUtils.isEmpty(userId)) throw new UserIdEmptyException();
        if (StringUtils.isEmpty(projectId)) throw new ProjectIdEmptyException();
        if (StringUtils.isEmpty(taskId)) throw new TaskIdEmptyException();
        if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
        final Task task = taskRepository.findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(projectId);
        return task;
    }

    @Override
    public Task unbindTaskFromProject(final String userId, final String projectId, final String taskId) {
        if (StringUtils.isEmpty(userId)) throw new UserIdEmptyException();
        if (StringUtils.isEmpty(projectId)) throw new ProjectIdEmptyException();
        if (StringUtils.isEmpty(taskId)) throw new TaskIdEmptyException();
        if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
        final Task task = taskRepository.findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(null);
        return task;
    }

    @Override
    public void removeProject(final String userId, final String projectId) {
        if (StringUtils.isEmpty(userId)) throw new UserIdEmptyException();
        if (StringUtils.isEmpty(projectId)) throw new ProjectIdEmptyException();
        final Project project = projectRepository.findOneById(userId, projectId);
        if (project == null) throw new ProjectNotFoundException();
        projectRepository.remove(userId, project);
        @NonNull final List<Task> tasks = taskRepository.findAllByProjectId(userId, projectId);
        tasks.stream().forEach(task -> taskRepository.remove(userId, task));
    }

    @Override
    public void removeProject(final String userId, final Project project) {
        if (StringUtils.isEmpty(userId)) throw new UserIdEmptyException();
        if (project == null) throw new ProjectNotFoundException();
        projectRepository.remove(userId, project);
        @NonNull final List<Task> tasks = taskRepository.findAllByProjectId(userId, project.getId());
        tasks.stream().forEach(task -> taskRepository.remove(userId, task));
    }

}

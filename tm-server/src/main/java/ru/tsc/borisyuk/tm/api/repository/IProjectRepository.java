package ru.tsc.borisyuk.tm.api.repository;

import lombok.NonNull;
import ru.tsc.borisyuk.tm.model.Project;

public interface IProjectRepository extends IUserOwnedRepository<Project> {

    @NonNull
    Project create(String userId, String name);

    @NonNull
    Project create(String userId, String name, String description);

    boolean existsById(String userId, String id);

}

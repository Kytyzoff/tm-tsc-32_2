package ru.tsc.borisyuk.tm.endpoint;

import lombok.NonNull;
import ru.tsc.borisyuk.tm.api.endpoint.ITaskEndpoint;
import ru.tsc.borisyuk.tm.api.service.IProjectTaskService;
import ru.tsc.borisyuk.tm.api.service.IServiceLocator;
import ru.tsc.borisyuk.tm.api.service.ITaskService;
import ru.tsc.borisyuk.tm.dto.request.*;
import ru.tsc.borisyuk.tm.dto.response.*;
import ru.tsc.borisyuk.tm.enumerated.Sort;
import ru.tsc.borisyuk.tm.enumerated.Status;
import ru.tsc.borisyuk.tm.model.Task;

import java.util.List;

public final class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    public TaskEndpoint(@NonNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NonNull
    private IProjectTaskService projectTaskService = getServiceLocator().getProjectTaskService();

    @NonNull
    private ITaskService taskService = getTaskService();

    @NonNull
    @Override
    public TaskChangeStatusByIdResponse taskChangeStatusById(@NonNull final TaskChangeStatusByIdRequest request) {
        check(request);
        final String id = request.getId();
        final String userId = request.getUserId();
        final Status status = request.getStatus();
        final Task task = getTaskService().changeStatusById(userId, id, status);
        return new TaskChangeStatusByIdResponse(task);
    }

    @NonNull
    @Override
    public TaskChangeStatusByIndexResponse taskChangeStatusByIndex(@NonNull final TaskChangeStatusByIndexRequest request) {
        check(request);
        final Integer index = request.getIndex();
        final String userId = request.getUserId();
        final Status status = request.getStatus();
        final Task task = getTaskService().changeStatusByIndex(userId, index, status);
        return new TaskChangeStatusByIndexResponse(task);
    }

    @NonNull
    @Override
    public TaskRemoveByIdResponse taskRemoveById(@NonNull final TaskRemoveByIdRequest request) {
        check(request);
        final String id = request.getId();
        final String userId = request.getUserId();
        final Task task = getTaskService().removeById(userId, id);
        return new TaskRemoveByIdResponse(task);
    }

    @NonNull
    @Override
    public TaskRemoveByIndexResponse taskRemoveByIndex(@NonNull final TaskRemoveByIndexRequest request) {
        check(request);
        final Integer index = request.getIndex();
        final String userId = request.getUserId();
        final Task task = getTaskService().removeByIndex(userId, index);
        return new TaskRemoveByIndexResponse(task);
    }

    @NonNull
    @Override
    public TaskGetByIdResponse taskGetById(@NonNull final TaskGetByIdRequest request) {
        check(request);
        final String id = request.getId();
        final String userId = request.getUserId();
        final Task task = getTaskService().findOneById(userId, id);
        return new TaskGetByIdResponse(task);
    }

    @NonNull
    @Override
    public TaskGetByIndexResponse taskGetByIndex(@NonNull final TaskGetByIndexRequest request) {
        check(request);
        final Integer index = request.getIndex();
        final String userId = request.getUserId();
        final Task task = getTaskService().findOneByIndex(userId, index);
        return new TaskGetByIndexResponse(task);
    }

    @NonNull
    @Override
    public TaskUpdateByIdResponse taskUpdateById(@NonNull final TaskUpdateByIdRequest request) {
        check(request);
        final String id = request.getId();
        final String userId = request.getUserId();
        final String name = request.getName();
        final String description = request.getDescription();
        final Task task = getTaskService().updateById(userId, id, name, description);
        return new TaskUpdateByIdResponse(task);
    }

    @NonNull
    @Override
    public TaskUpdateByIndexResponse taskUpdateByIndex(@NonNull final TaskUpdateByIndexRequest request) {
        check(request);
        final Integer index = request.getIndex();
        final String userId = request.getUserId();
        final String name = request.getName();
        final String description = request.getDescription();
        final Task task = getTaskService().updateByIndex(userId, index, name, description);
        return new TaskUpdateByIndexResponse(task);
    }

    @NonNull
    @Override
    public TaskCreateResponse taskCreate(@NonNull final TaskCreateRequest request) {
        check(request);
        final String userId = request.getUserId();
        final String name = request.getName();
        final String description = request.getDescription();
        final Task task = getTaskService().create(userId, name, description);
        return new TaskCreateResponse(task);
    }

    @NonNull
    @Override
    public TaskListResponse taskList(@NonNull final TaskListRequest request) {
        check(request);
        final String userId = request.getUserId();
        final Sort sort = request.getSort();
        final List<Task> tasks = getTaskService().findAll(userId, sort);
        return new TaskListResponse(tasks);
    }

    @NonNull
    @Override
    public TaskClearResponse taskClear(@NonNull final TaskClearRequest request) {
        check(request);
        final String userId = request.getUserId();
        getTaskService().clear(userId);
        return new TaskClearResponse();
    }

    @NonNull
    @Override
    public TaskBindToProjectResponse taskBindToProject(@NonNull final TaskBindToProjectRequest request) {
        final String userId = request.getUserId();
        final String projectId = request.getProjectId();
        final String taskId = request.getTaskId();
        final Task task = projectTaskService.bindTaskToProject(userId, projectId, taskId);
        return new TaskBindToProjectResponse(task);
    }

    @NonNull
    @Override
    public TaskUnbindFromProjectResponse taskUnbindFromProject(@NonNull final TaskUnbindFromProjectRequest request) {
        final String userId = request.getUserId();
        final String projectId = request.getProjectId();
        final String taskId = request.getTaskId();
        final Task task = projectTaskService.unbindTaskFromProject(userId, projectId, taskId);
        return new TaskUnbindFromProjectResponse(task);
    }

}

package ru.tsc.borisyuk.tm.service;

import lombok.NonNull;
import org.apache.commons.lang3.StringUtils;
import ru.tsc.borisyuk.tm.api.service.IAuthService;
import ru.tsc.borisyuk.tm.api.service.IPropertyService;
import ru.tsc.borisyuk.tm.api.service.IUserService;
import ru.tsc.borisyuk.tm.exception.field.LoginEmptyException;
import ru.tsc.borisyuk.tm.exception.field.PasswordEmptyException;
import ru.tsc.borisyuk.tm.exception.system.AccessDeniedException;
import ru.tsc.borisyuk.tm.model.User;
import ru.tsc.borisyuk.tm.util.HashUtil;

public class AuthService implements IAuthService {

    @NonNull
    private final IUserService userService;

    @NonNull
    private final IPropertyService propertyService;

    public AuthService(
            @NonNull final IPropertyService propertyService,
            @NonNull final IUserService userService
    ) {
        this.propertyService = propertyService;
        this.userService = userService;
    }

    @Override
    public User register(final String login, final String password, final String email) {
        return userService.create(login, password, email);
    }

    @Override
    public User check(final String login, final String password) {
        if (StringUtils.isEmpty(login)) throw new LoginEmptyException();
        if (StringUtils.isEmpty(password)) throw new PasswordEmptyException();
        final User user = userService.findOneByLogin(login);
        if (user == null) throw new AccessDeniedException();
        if (user.isLocked()) throw new AccessDeniedException();
        if (!user.getPasswordHash().equals(HashUtil.salt(propertyService, password)))
            throw new AccessDeniedException();
        return user;
    }

}

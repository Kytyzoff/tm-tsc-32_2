package ru.tsc.borisyuk.tm.component;

import lombok.Getter;
import lombok.NonNull;
import lombok.SneakyThrows;
import ru.tsc.borisyuk.tm.api.component.Operation;
import ru.tsc.borisyuk.tm.dto.request.AbstractRequest;
import ru.tsc.borisyuk.tm.dto.response.AbstractResponse;
import ru.tsc.borisyuk.tm.task.AbstractServerTask;
import ru.tsc.borisyuk.tm.task.ServerAcceptTask;

import java.net.ServerSocket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public final class Server {

    @Getter
    @NonNull
    private final Bootstrap bootstrap;

    @NonNull
    private final Dispatcher dispatcher = new Dispatcher();

    @NonNull
    private final ExecutorService executorService = Executors.newCachedThreadPool();

    @Getter
    private ServerSocket serverSocket;

    public Server(@NonNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    @SneakyThrows
    public void start() {
        @NonNull final Integer port = bootstrap.getPropertyService().getServerPort();
        serverSocket = new ServerSocket(port);
        submit(new ServerAcceptTask(this));
    }

    @SneakyThrows
    public void stop() {
        if (serverSocket == null) return;
        serverSocket.close();
        executorService.shutdown();
    }

    public void submit(@NonNull final AbstractServerTask task) {
        executorService.submit(task);
    }

    public <RQ extends AbstractRequest, RS extends AbstractResponse> void register(final Class<RQ> clazz, final Operation<RQ, RS> operation) {
        dispatcher.register(clazz, operation);
    }

    public AbstractResponse call(final AbstractRequest request) {
        return dispatcher.call(request);
    }

}

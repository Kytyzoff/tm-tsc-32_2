package ru.tsc.borisyuk.tm.component;

import ru.tsc.borisyuk.tm.api.component.Operation;
import ru.tsc.borisyuk.tm.dto.request.AbstractRequest;
import ru.tsc.borisyuk.tm.dto.response.AbstractResponse;

import java.util.LinkedHashMap;
import java.util.Map;

public class Dispatcher {

    private final Map<Class<? extends AbstractRequest>, Operation<? extends AbstractRequest, ? extends AbstractResponse>> map = new LinkedHashMap<>();

    public <RQ extends AbstractRequest, RS extends AbstractResponse> void register(Class<RQ> clazz, Operation<RQ, RS> operation) {
        map.put(clazz, operation);
    }

    public AbstractResponse call(AbstractRequest request) {
        final Operation operation = map.get(request.getClass());
        if (operation == null) return null;
        return operation.execute(request);
    }

}

package ru.tsc.borisyuk.tm.api.service;

import lombok.NonNull;
import ru.tsc.borisyuk.tm.api.component.ISaltProvider;

public interface IPropertyService extends ISaltProvider {

    @NonNull
    String getApplicationVersion();

    @NonNull
    String getAuthorName();

    @NonNull
    String getAuthorEmail();

    @NonNull
    String getDataFileBinary();

    @NonNull
    String getDataFileBase64();

    @NonNull
    String getDataFileJaxbXml();

    @NonNull
    String getDataFileJaxbJson();

    @NonNull
    String getDataFileFasterXml();

    @NonNull
    String getDataFileFasterJson();

    @NonNull
    String getDataFileFasterYaml();

    @NonNull
    String getDataFileBackupXml();

    @NonNull
    Integer getServerPort();

}

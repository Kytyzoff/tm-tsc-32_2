package ru.tsc.borisyuk.tm.endpoint;

import lombok.NonNull;
import ru.tsc.borisyuk.tm.api.endpoint.IProjectEndpoint;
import ru.tsc.borisyuk.tm.api.service.IServiceLocator;
import ru.tsc.borisyuk.tm.dto.request.*;
import ru.tsc.borisyuk.tm.dto.response.*;
import ru.tsc.borisyuk.tm.enumerated.Sort;
import ru.tsc.borisyuk.tm.enumerated.Status;
import ru.tsc.borisyuk.tm.model.Project;

import java.util.List;

public final class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    public ProjectEndpoint(@NonNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NonNull
    @Override
    public ProjectChangeStatusByIdResponse projectChangeStatusById(@NonNull final ProjectChangeStatusByIdRequest request) {
        check(request);
        final String id = request.getId();
        final String userId = request.getUserId();
        final Status status = request.getStatus();
        final Project project = getProjectService().changeStatusById(userId, id, status);
        return new ProjectChangeStatusByIdResponse(project);
    }

    @NonNull
    @Override
    public ProjectChangeStatusByIndexResponse projectChangeStatusByIndex(@NonNull final ProjectChangeStatusByIndexRequest request) {
        check(request);
        final Integer index = request.getIndex();
        final String userId = request.getUserId();
        final Status status = request.getStatus();
        final Project project = getProjectService().changeStatusByIndex(userId, index, status);
        return new ProjectChangeStatusByIndexResponse(project);
    }

    @NonNull
    @Override
    public ProjectRemoveByIdResponse projectRemoveById(@NonNull final ProjectRemoveByIdRequest request) {
        check(request);
        final String id = request.getId();
        final String userId = request.getUserId();
        final Project project = getProjectService().removeById(userId, id);
        return new ProjectRemoveByIdResponse(project);
    }

    @NonNull
    @Override
    public ProjectRemoveByIndexResponse projectRemoveByIndex(@NonNull final ProjectRemoveByIndexRequest request) {
        check(request);
        final Integer index = request.getIndex();
        final String userId = request.getUserId();
        final Project project = getProjectService().removeByIndex(userId, index);
        return new ProjectRemoveByIndexResponse(project);
    }

    @NonNull
    @Override
    public ProjectGetByIdResponse projectGetById(@NonNull final ProjectGetByIdRequest request) {
        check(request);
        final String id = request.getId();
        final String userId = request.getUserId();
        final Project project = getProjectService().findOneById(userId, id);
        return new ProjectGetByIdResponse(project);
    }

    @NonNull
    @Override
    public ProjectGetByIndexResponse projectGetByIndex(@NonNull final ProjectGetByIndexRequest request) {
        check(request);
        final Integer index = request.getIndex();
        final String userId = request.getUserId();
        final Project project = getProjectService().findOneByIndex(userId, index);
        return new ProjectGetByIndexResponse(project);
    }

    @NonNull
    @Override
    public ProjectUpdateByIdResponse projectUpdateById(@NonNull final ProjectUpdateByIdRequest request) {
        check(request);
        final String id = request.getId();
        final String userId = request.getUserId();
        final String name = request.getName();
        final String description = request.getDescription();
        final Project project = getProjectService().updateById(userId, id, name, description);
        return new ProjectUpdateByIdResponse(project);
    }

    @NonNull
    @Override
    public ProjectUpdateByIndexResponse projectUpdateByIndex(@NonNull final ProjectUpdateByIndexRequest request) {
        check(request);
        final Integer index = request.getIndex();
        final String userId = request.getUserId();
        final String name = request.getName();
        final String description = request.getDescription();
        final Project project = getProjectService().updateByIndex(userId, index, name, description);
        return new ProjectUpdateByIndexResponse(project);
    }

    @NonNull
    @Override
    public ProjectCreateResponse projectCreate(@NonNull final ProjectCreateRequest request) {
        check(request);
        final String userId = request.getUserId();
        final String name = request.getName();
        final String description = request.getDescription();
        final Project project = getProjectService().create(userId, name, description);
        return new ProjectCreateResponse(project);
    }

    @NonNull
    @Override
    public ProjectListResponse projectList(@NonNull final ProjectListRequest request) {
        check(request);
        final String userId = request.getUserId();
        final Sort sort = request.getSort();
        final List<Project> projects = getProjectService().findAll(userId, sort);
        return new ProjectListResponse(projects);
    }

    @NonNull
    @Override
    public ProjectClearResponse projectClear(@NonNull final ProjectClearRequest request) {
        check(request);
        final String userId = request.getUserId();
        getProjectService().clear(userId);
        return new ProjectClearResponse();
    }

}

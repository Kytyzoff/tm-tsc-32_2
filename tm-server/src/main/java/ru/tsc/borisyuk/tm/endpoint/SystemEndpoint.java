package ru.tsc.borisyuk.tm.endpoint;

import lombok.NonNull;
import ru.tsc.borisyuk.tm.api.endpoint.ISystemEndpoint;
import ru.tsc.borisyuk.tm.api.service.IPropertyService;
import ru.tsc.borisyuk.tm.api.service.IServiceLocator;
import ru.tsc.borisyuk.tm.dto.request.ServerAboutRequest;
import ru.tsc.borisyuk.tm.dto.request.ServerVersionRequest;
import ru.tsc.borisyuk.tm.dto.response.ServerAboutResponse;
import ru.tsc.borisyuk.tm.dto.response.ServerVersionResponse;

public final class SystemEndpoint implements ISystemEndpoint {

    @NonNull
    private final IServiceLocator serviceLocator;

    public SystemEndpoint(@NonNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    @NonNull
    public ServerAboutResponse getAbout(@NonNull final ServerAboutRequest request) {
        @NonNull final IPropertyService propertyService = serviceLocator.getPropertyService();
        @NonNull final ServerAboutResponse response = new ServerAboutResponse();
        response.setEmail(propertyService.getAuthorEmail());
        response.setName(propertyService.getAuthorName());
        return response;
    }

    @Override
    @NonNull
    public ServerVersionResponse getVersion(@NonNull final ServerVersionRequest request) {
        @NonNull final IPropertyService propertyService = serviceLocator.getPropertyService();
        @NonNull final ServerVersionResponse response = new ServerVersionResponse();
        response.setVersion(propertyService.getApplicationVersion());
        return response;
    }

}
